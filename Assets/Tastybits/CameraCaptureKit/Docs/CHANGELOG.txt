
=========================
Version 1.3
==========================

Added Android AAR instead of JAR to help resolve issues with merging the Android Manifest automaticly.


Fixed issue in Unity 5.4 Android Player. The camera didn’t initialise.

Fixed issue with crash on some Android devices not supporting the same capture mode as the set preview mode.

Introduced a Quick Snap mode to make the snapshot quicker.

Introduced the ability to use your own WebCamTexture.


==========================
Version 1.2
==========================

Fixed issue where Multiple Android plugins would conflict and the Android plugin would throw an exception
making it impossible to start the camera.

Added Utility class Documents for saving a loading Textures taken with the camera.

Added method in Documents to save a texture to the Photo Album making it accessable from other apps.

Removed Debug.Print's that was taking up too much space in the log.

TODO:  Added GetSupportedFlashModeNames functionality to get the names of the supported FlashModes on the device.
TODO: Make a view that reports all supported flash modes.

Added a bugfix on some Android models, flash/autoflash functionality wasnt reported.

TODO: Supported setting Flash mode, ISO/Exposure in OnGUI demo

TODO: Supported Getting Supported Camera properties and showing them in the Demo.



==========================
Version 1.1
==========================

Fixed various minor issues on iOS : 

Some people experienced a linker error as well as problems with the orientation of the camera.

Added support for adjusting ISO and Exposure Compensation for Android and iOS.


==========================
Version 1.0
==========================

Initial Release of the Asset for AssetStore.

Feature set to be able to plugin and play still image image capturing in Unity.

Two demo's one for Unity.UI and one for OnGUI.  







