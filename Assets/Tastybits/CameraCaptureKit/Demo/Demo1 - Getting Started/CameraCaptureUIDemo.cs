﻿ using UnityEngine;
using System.Collections;
using Tastybits.CamCap;


/*
	==================================================================
	  Camera Capture Kit UI Demo ( Prefab Demo )
 	==================================================================

   	This demo shows you how to get up and running quickly with the camera and it's UI
	prefab.

	How this works :

	1. Drag the "Camera Capture UI" prefab into an existing UI Canvas hierarchy. 

	2. Make some code that starts the Camera UI - For an example have a look at this class "CameraCaptureUIDemo" - it shows
	you how to start the camera UI and capture an image. You can extend it or copy the functionality into your own class.
*/
public class CameraCaptureUIDemo : MonoBehaviour {
	enum State {
		Wait = -1,
		Begin = 0,
		Capturing = 1,
		ShowResult = 3
	}
	public Texture2D finalImage;
	public GameObject spinningCube;
	public Material materialOfCube;
	State state=State.Begin;
	public CameraCaptureUI cameraCaptureUI;
	public bool saveCapturedImage = true;
	public UnityEngine.UI.RawImage lastImageTaken;


	Texture2D _previous=null;
	Texture2D previous {
		get {
			string prevSavedName = PlayerPrefs.GetString("prevSavedName","");
			if( _previous == null && !string.IsNullOrEmpty(prevSavedName) ) {
				Documents.LoadTextureFromDocuments( prevSavedName,out _previous);
			}
			return _previous;
		}
	}

	void Awake() {
		if( cameraCaptureUI == null ) {
			Debug.LogError("CameraCaptureUI of CameraCaptureDemo was not specified.");
		}
	}


	void Start() {
		cameraCaptureUI.gameObject.SetActive(false);
		spinningCube.SetActive(false);
	}


	void SetLastImageTaken() {
		if( previous == null ) {
			return;// no last image
		}
		if( this.lastImageTaken!=null ) {
			this.lastImageTaken.texture = previous;
		}
	}


	public void OnStartCameraNoInspectClicked(){
		state = State.Wait;
		cameraCaptureUI.OnPhotoCaptured = this.OnPhotoCaptured;
		cameraCaptureUI.SetActiveAndStartCapture( false, ( bool ok )=>{
			state = ok ? State.Capturing : State.Begin;
			if( ! ok )  {
				Debug.LogError("Error starting camera.");
			} else {
			}
		});
	}


	public void OnStartCameraClicked(){
		state = State.Wait;
		cameraCaptureUI.OnPhotoCaptured = this.OnPhotoCaptured;
		cameraCaptureUI.SetActiveAndStartCapture( true, ( bool ok )=>{
			state = ok ? State.Capturing : State.Begin;
			if( ! ok )  {
				Debug.LogError("Error starting camera.");
			} else {
			}
		});
	}


	string _statusText;
	string StatusText {
		get {
			return _statusText;
		}
		set { 
			
			_statusText = value;
		}
	}


	void Update() {
		if( state==State.Begin ) {
			StatusText = "This demo shows you how to use the UI Prefab.\nThe UI Prefab is intended to be dragged into an existing UI Canvas and then use it as is or modify it for your needs.";
			SetLastImageTaken();
			finalImage = null;
			spinningCube.SetActive(false);
		} else if( state == State.ShowResult ) {
			StatusText = "This is the texture captured.\nHere the texture is used to display on a spinning cube.\nBut you can use it for alot of diffirent things.";
			finalImage = null;
			spinningCube.SetActive(true);
		}
	}


	public void OnSaveToAlbumAndRestartClicked() {
		Documents.SaveTextureToPhotoAlbum( "savedimage.png", finalImage );
		spinningCube.SetActive(false);
		state = State.Begin;
	}


	public void OnSaveToDocumentsAndRestartClicked(){
		Documents.SaveTextureToDocuments( "savedimage.png", finalImage );
		PlayerPrefs.SetString("prevSavedName","savedimage.png");
		spinningCube.SetActive(false);
		state = State.Begin;
	}


	public void OnRestartClicked(){
		state = State.Begin;
		spinningCube.SetActive(false);
	}


	/*public void OnRestartClicked(){
		state = State.Begin;
		spinningCube.SetActive(false);
	}*/


	public bool showResultingPhotoOnCube = true;

	// Callback called when the photo has been taken and ready for you to use 
	// in this example we set the maintexture of a material to point at it.
	void OnPhotoCaptured( Texture2D tex ) {
		Debug.Log("DEMO: OnPhotoCaptured" );
		state = State.ShowResult;	
		finalImage = tex;

		if( finalImage != null && this.saveCapturedImage ) {
			Debug.Log( "DEMO: Automaticly saved image to documents( for testing )");
			Documents.SaveTextureToDocuments( "savedimage.png", finalImage );
			PlayerPrefs.SetString("prevSavedName","savedimage.png");
			_previous = null; // reset this so we will load the new one.
		}
		if( showResultingPhotoOnCube ) {
			materialOfCube.mainTexture = finalImage;
			spinningCube.SetActive(true);
			cameraCaptureUI.gameObject.SetActive(false);
		} else {
			spinningCube.SetActive(false);
			cameraCaptureUI.gameObject.SetActive(true); 
		}
	}


	// For testing.
	void DrawDebugLabel() {
		var webCamTexture = cameraCaptureUI.controller.WebCamTexture;
		int vra = -1;
		int wi = -1;
		int he = -1;
		int rw = -1;
		int rh = -1;
		if( webCamTexture!=null ) {
			vra=webCamTexture.videoRotationAngle;
			wi = webCamTexture.width;
			he = webCamTexture.height;
			rw = webCamTexture.requestedWidth;
			rh = webCamTexture.requestedHeight;
		}
		bool isBack = cameraCaptureUI.controller.IsCurrentBackFacing()==false;
		GUI.Label( new Rect(0f,Screen.height-30f,Screen.width,30f), "VRA:"+ vra + " BackF:"+isBack + " WebCamTexDims:" + wi +  "," + he + " reqWH=" + rw+"," +rh  );
	}


	// GUIStyle used when drawing describtion label.
	GUIStyle labelStyle;
	GUIStyle LabelStyle {
		get {
			if( labelStyle==null ) {
				labelStyle = new GUIStyle();
				labelStyle = GUI.skin.label;
				labelStyle.richText=true;
				labelStyle.fontStyle = FontStyle.Bold;
				labelStyle.alignment = TextAnchor.MiddleCenter;
				var _tex = new Texture2D(2,2); 
				_tex.SetPixels( new Color[]{new Color(0,0,0,0.25f),new Color(0,0,0,0.25f),new Color(0,0,0,0.25f),new Color(0,0,0,0.25f) } );
				_tex.name = "Black TEx";
				_tex.Apply();
				labelStyle.normal.background = _tex;

				labelStyle.fontSize = 15;
				if( Screen.width > 1000 || Screen.height > 1000 ) {
					labelStyle.fontSize = 30;
				}

			}
			return labelStyle;
		}
	}

	// The style of the buttons used.
	GUIStyle buttonStyle;
	GUIStyle ButtonStyle {
		get {
			if( buttonStyle == null ) {
				buttonStyle = new GUIStyle( GUI.skin.button );
				int newFontSz = 8;
				buttonStyle.fontSize = buttonStyle.fontSize * newFontSz; 
			}
			return buttonStyle;
		}
	}




}
