﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class OpenTargetScene : MonoBehaviour {

    public string mTargetSceneName;

    public void OpenScene() {
        SceneManager.LoadScene(mTargetSceneName);
    }
}
