﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TaskBlock : ScrollViewBlock {

    public delegate void TaskBlockOpened(TaskBlock block);
    public static event TaskBlockOpened TaskOpened;
    public static void OnTaskOpened(TaskBlock block) {
        if (TaskOpened != null) {
            TaskOpened.Invoke(block);
        }
    }

    void Start() {
        TaskOpened += CloseAllOtherTasks;
    }

    void OnDestroy() {
        TaskOpened -= CloseAllOtherTasks;
    }

    public void CloseAllOtherTasks(TaskBlock block) {
        if(this != block) {
            Collapse();
        }
    }

    public override void AddtionalFeatures(bool expand) {
        if (expand) {
            OnTaskOpened(this);
        }
    }
}
