﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoryBlock : ScrollViewBlock {

    public delegate void StoryBlockOpened(StoryBlock block);
    public static event StoryBlockOpened StoryOpened;
    public static void OnStoryOpened(StoryBlock block) {
        if (StoryOpened != null) {
            StoryOpened.Invoke(block);
        }
    }

    void Start() {
        StoryOpened += CloseAllOtherStories;
    }

    void OnDestroy() {
        StoryOpened -= CloseAllOtherStories;
    }

    public override void AddtionalFeatures(bool expand) {
        if (expand) {
            OnStoryOpened(this);
        }
    }

    public void CloseAllOtherStories(StoryBlock block) {
        if(this != block) {
            Collapse();
        }
    }
    
}
