﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonImageToggle : MonoBehaviour {

    public List<Image> mImages;

    public void Toggle() {
        for(int i = 0; i < mImages.Count; i++) {
            mImages[i].enabled = !mImages[i].enabled;
        }
    }
}
