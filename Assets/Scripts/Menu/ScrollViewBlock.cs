﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollViewBlock : MonoBehaviour {

    public LayoutElement mLayoutEle;
    public Sprite mExpandedImage;
    public Sprite mCollapsedImage;
    public GameObject mExpandIcon;
    public GameObject mCollapseIcon;

    public float mExpandedHeight;
    private float mCollapsedHeight;

    public List<GameObject> mExpandedObjectsToReveal;

    void Awake() {
        mCollapsedHeight = mLayoutEle.minHeight;
    }
  

    public void Expand() {

        SetLayoutSize(true);
        mExpandIcon.SetActive(false);
        mCollapseIcon.SetActive(true);
        this.GetComponent<Image>().sprite = mExpandedImage;
        for (int i = 0; i < mExpandedObjectsToReveal.Count; i++) {
            mExpandedObjectsToReveal[i].SetActive(true);
        }
        AddtionalFeatures(true);       

    }

    public virtual void AddtionalFeatures(bool expand) {

    }   

    public void Collapse() {

        SetLayoutSize(false);

        if(mExpandIcon != null) {
            mExpandIcon.SetActive(true);
        }else {
            Debug.Log(gameObject.name);
        }

        if(mCollapseIcon != null) {
            mCollapseIcon.SetActive(false);
        }else {
            Debug.Log(gameObject.name);
        }        
        
        this.GetComponent<Image>().sprite = mCollapsedImage;
        for (int i = 0; i < mExpandedObjectsToReveal.Count; i++) {
            mExpandedObjectsToReveal[i].SetActive(false);
        }
        AddtionalFeatures(false);
    }

    private void SetLayoutSize(bool expand) {

        if(mLayoutEle != null) {
            float height;

            if (expand) {
                height = mExpandedHeight;
            }
            else {
                height = mCollapsedHeight;
            }

            mLayoutEle.preferredHeight = height;
            mLayoutEle.minHeight = height;
        }
    }
}
