﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateAddtionalInfoPanel : MonoBehaviour {

    public AdditionalInformationPanel mAdditionalInfoPanel;

    public List<string> mInfoTexts;
    public List<Sprite> mInfoSprites;
  
    public void UpdatePanel() {
        if (mAdditionalInfoPanel)
            mAdditionalInfoPanel.UpdatePanel(mInfoTexts, mInfoSprites);
    }
}
