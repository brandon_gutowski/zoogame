﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class SelectPicture : MonoBehaviour {

    public List<GameObject> mPictures;
    public PuzzlePage mPuzzlePage;    

    protected int mSelectedPictureInt = -1;
    protected List<int> mDisabledPictures = new List<int>();

    public void Select(int incomingInt) {
        mSelectedPictureInt = incomingInt;
        Debug.Assert((mSelectedPictureInt) > -1);
        FollowUpSelect(incomingInt);
    }

    public void DisablePicture(int num) {

        for (int i = 0; i < mDisabledPictures.Count; i++) {
            if (mDisabledPictures[i] == num) {
                return;
            }
        }

        mDisabledPictures.Add(num);
        mPictures[num].GetComponent<Button>().enabled = false;
        FollowUpDisable(num);
    }

    protected virtual void FollowUpSelect(int incomingInt) {

    }

    protected virtual void FollowUpDisable(int num) {

    }

    protected bool PictureIsDisabled(int num) {

        for (int i = 0; i < mDisabledPictures.Count; i++) {
            if (mDisabledPictures[i] == num) {
                return true;
            }
        }

        return false;
    }
}
