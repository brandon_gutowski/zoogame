﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Tastybits.CamCap;

public class PicturePuzzle : SelectPicture {

    public List<RawImage> mCameraPictures;
    public List<Vector2> mGPSRequirements;

	protected override void FollowUpSelect(int num) {

        CameraCaptureUI.CreateUIAndCaptureImage((Texture2D capturedPhoto) => {
            mCameraPictures[num].texture = capturedPhoto;
        });
        mCameraPictures[num].transform.parent.gameObject.SetActive(true);
        mPictures[num].SetActive(false);
    
       // Color color = mCameraPictures[num].color;
       // color.a = 256;
       // mCameraPictures[num].color = color;
    }

    //GPS Checks will be done once we know we can get a camera Image

}
