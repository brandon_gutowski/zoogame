﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ExplainationPanel : MonoBehaviour {

    [TextArea(3, 10)]
    public List<string> mExplainations; //should correspond to each picture.  0 to 0, 1 to 1, etc
    public Text mText;
    public PuzzlePage mPuzzlePage;

    private bool mSelectedCorrectPicture = false;


	public void Open(bool isCorrectPicture) {
        mSelectedCorrectPicture = isCorrectPicture;
        this.GetComponent<OpenOrClosePanel>().Open();
    }

    public void UpdateText(int pictureInt) {
        mText.text = mExplainations[pictureInt];
    }

    public void Close() {
        mPuzzlePage.CheckPuzzle(mSelectedCorrectPicture);
    }
}
