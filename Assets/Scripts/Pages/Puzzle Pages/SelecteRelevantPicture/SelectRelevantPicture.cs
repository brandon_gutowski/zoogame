﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class SelectRelevantPicture : SelectPicture {

    public int mCorrectPictureInt;
    public ExplainationPanel mExplainationPanel;
    public int mScrollViewSelection = 0;
    [SerializeField]
    private int mNumberOfPictures = 4;

    //Eventually add a highlight color or effect here

    public void SubmitAnswer() {

        if(mSelectedPictureInt > -1) {
            if(mSelectedPictureInt == mCorrectPictureInt) {
                mExplainationPanel.UpdateText(mSelectedPictureInt);
                mExplainationPanel.Open(true);
            }
            else {
                mExplainationPanel.UpdateText(mSelectedPictureInt);
                mExplainationPanel.Open(false);
                DisablePicture(mSelectedPictureInt);
            }
        }
        else {

            if (mScrollViewSelection == mCorrectPictureInt) {
                mExplainationPanel.UpdateText(mScrollViewSelection);
                mExplainationPanel.Open(true);
            }
            else {
                mExplainationPanel.UpdateText(mScrollViewSelection);
                mExplainationPanel.Open(false);
                DisablePicture(mScrollViewSelection);
            }
        }
    }

    public void ScrollViewMovedRight() {

        if(mScrollViewSelection < mNumberOfPictures - 1) {
            mScrollViewSelection++;
        }
    }

    public void ScrollViewMovedLeft() {

        if (mScrollViewSelection > 0) {
            mScrollViewSelection--;
        }
    }

    protected override void FollowUpSelect(int incomingInt) {
        HighlightOnePicture(mSelectedPictureInt);
    }

    protected override void FollowUpDisable(int num) {
        mPictures[num].transform.parent.GetComponent<Image>().color = Color.gray;
    }

    private void HighlightOnePicture(int id) {
        Debug.Log("Highlight: " + id);
        for(int i = 0; i < mPictures.Count; i++) {
            if (!PictureIsDisabled(i)) {
                if (i == id) {
                    mPictures[i].transform.parent.GetComponent<Image>().color = Color.green;
                }
                else {
                    mPictures[i].transform.parent.GetComponent<Image>().color = Color.white;
                }
            }
        }
    }

    
}
