﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GridManager : MonoBehaviour {

    public int mRightPositionLimit = -2600;

    public void MoveRight() {

        Vector3 newPosition = this.gameObject.GetComponent<RectTransform>().localPosition;    
        newPosition.x -= MoveAmount();

        if (this.gameObject.GetComponent<RectTransform>().localPosition.x > mRightPositionLimit) {           
            this.gameObject.GetComponent<RectTransform>().localPosition = newPosition;
        }        
    }

    public void MoveLeft() {
      
        Vector3 newPosition = this.gameObject.GetComponent<RectTransform>().localPosition;
        
        newPosition.x += MoveAmount();
 
        if (this.gameObject.GetComponent<RectTransform>().localPosition.x < 0) {
            this.gameObject.GetComponent<RectTransform>().localPosition = newPosition;
        }
    }

    private float MoveAmount() {
        float moveAmount = this.gameObject.GetComponent<GridLayoutGroup>().spacing.x + this.gameObject.GetComponent<GridLayoutGroup>().cellSize.x;
        return moveAmount;
    }
}
