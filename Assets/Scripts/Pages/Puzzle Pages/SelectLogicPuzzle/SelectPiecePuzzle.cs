﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectPiecePuzzle : PuzzlePage {

    public List<SelectPuzzlePiece> mPuzzlePieces;
    public List<SelectPuzzlePiece> mPuzzlePieceAnswers;
    public int mRequiredSelections;
    public Color mSelectionColor;

    protected List<SelectPuzzlePiece> mSelectedPieces = new List<SelectPuzzlePiece>();

    public virtual void CheckAnswers() {

        List<bool> isAnswerPresent = new List<bool>(mPuzzlePieceAnswers.Count);

        for(int i = 0; i < mPuzzlePieceAnswers.Count; i++) {
            isAnswerPresent[i] = false;
            for(int j = 0; j < mSelectedPieces.Count; j++) {
                if(mSelectedPieces[j] == mPuzzlePieceAnswers[i]) {
                    isAnswerPresent[i] = true;
                }
            }
        }

        bool areAllAnswersPresent = true;
        
        for(int i = 0; i < isAnswerPresent.Count; i++) {
            if (!isAnswerPresent[i]) {
                areAllAnswersPresent = false;
            }
        }

        CheckPuzzle(areAllAnswersPresent);

    }

    public virtual Color Select(SelectPuzzlePiece piece){

        if(mPuzzlePieceAnswers.Count == 1) {
            mSelectedPieces[0] = piece;
            DeselectAllBut(piece);
#if UNITY_ANDROID
            Handheld.Vibrate();
#endif
            return mSelectionColor;
        }
        else if (mRequiredSelections > mSelectedPieces.Count) {
            mSelectedPieces.Add(piece);
#if UNITY_ANDROID
            Handheld.Vibrate();
#endif
            return mSelectionColor;
        }       
        
        return Color.white;
    }

    public virtual void Deselect(SelectPuzzlePiece piece) {
        for (int i = 0; i < mSelectedPieces.Count; i++) {
            if (piece == mSelectedPieces[i]) {
                mSelectedPieces.Remove(piece);
                piece.DeselectPiece();
            }
        }
    }

    public void DeselectAllBut(SelectPuzzlePiece piece) {
        for (int i = 0; i < mPuzzlePieces.Count; i++) {
            if (piece != mPuzzlePieces[i]) {
                mPuzzlePieces[i].DeselectPiece();
            }
        }
    }

    //Change this to returns ints.
    public List<SelectPuzzlePiece> GetSelectedPieces() {
        return mSelectedPieces;
    }
    public void ClearSelectedPieces() {
        mSelectedPieces = new List<SelectPuzzlePiece>();
    }

}
