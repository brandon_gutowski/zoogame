﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwapPiecePuzzle : SelectPiecePuzzle {

    //Swap Pairs mean that if you have 3 things that need to swap places, both lists will be 3 long
    //pair 1[0] pairs to Pair2[0]
    public SelectPuzzlePiece[] mAnswerSwapPair1;
    public SelectPuzzlePiece[] mAnswerSwapPair2;
    public List<Color> mSelectionColors;

    private SelectPuzzlePiece[] mSelectedSwapPair1;
    private SelectPuzzlePiece[] mSelectedSwapPair2;

    void Awake() {

        Debug.Assert(mSelectionColors.Count == mAnswerSwapPair1.Length);
        Debug.Assert(mAnswerSwapPair1.Length == mAnswerSwapPair2.Length);

        mSelectedSwapPair1 = new SelectPuzzlePiece[mAnswerSwapPair1.Length];
        mSelectedSwapPair2 = new SelectPuzzlePiece[mAnswerSwapPair2.Length];

    }

    public override void CheckAnswers() {

        bool[] isAnswerPresent = CheckIfSelectedPairsMatchAnswers();

        bool areAllAnswersPresent = true;

        for (int i = 0; i < isAnswerPresent.Length; i++) {
            if (!isAnswerPresent[i]) {
                areAllAnswersPresent = false;
            }
        }
        
        CheckPuzzle(areAllAnswersPresent);

    }

    public override Color Select(SelectPuzzlePiece piece) {

        
        int[] selectionSlot = ChooseSelectionSlot();

        switch (selectionSlot[0]){

            case 0:
                mSelectedSwapPair1[selectionSlot[1]] = piece;
#if UNITY_ANDROID
                Handheld.Vibrate();
#endif
                return mSelectionColors[selectionSlot[1]];

            case 1:
                mSelectedSwapPair2[selectionSlot[1]] = piece;
#if UNITY_ANDROID
                Handheld.Vibrate();
#endif
                return mSelectionColors[selectionSlot[1]];

            default:
                //All slots are full
                return Color.white;
        }
    }

    public override void Deselect(SelectPuzzlePiece piece) {

        bool haveDeselected = false;

        for (int i = 0; i < mSelectedSwapPair1.Length; i++) {
            if (piece == mSelectedSwapPair1[i]) {
                mSelectedSwapPair1[i] = null;
                piece.DeselectPiece();
                haveDeselected = true;
            }
        }

        if (!haveDeselected) {
            for (int i = 0; i < mSelectedSwapPair2.Length; i++) {
                if (piece == mSelectedSwapPair2[i]) {
                    mSelectedSwapPair2[i] = null;
                    piece.DeselectPiece();
                    haveDeselected = true;
                }
            }
        }
    }

    private int[] ChooseSelectionSlot() {

        int[] slot = { -1, -1 };

        for (int i = 0; i < mSelectedSwapPair1.Length; i++) {
            if(mSelectedSwapPair1[i] == null) {
                slot = new int[] { 0, i };
            }else if(mSelectedSwapPair2[i] == null){
                slot = new int[] { 1, i };
            }
        }      

        return slot;

    }

    private bool[] CheckIfSelectedPairsMatchAnswers() {
        bool[] isAnswerPresent = new bool[mAnswerSwapPair1.Length];

        //I need to check both list, if you find an answer, you check the matching pair in the other list.  Then set that bool in the isAnswerPresent as true
        for (int i = 0; i < mAnswerSwapPair1.Length; i++) {
            isAnswerPresent[i] = false;
            for (int j = 0; j < mSelectedSwapPair1.Length; j++) {
                if (mSelectedSwapPair1[j] != null) {
                    if (mSelectedSwapPair1[j] == mAnswerSwapPair1[i]) {
                        if (mSelectedSwapPair2[j] == mAnswerSwapPair2[i]) {           
                           isAnswerPresent[i] = true;
                        }
                    }else if(mSelectedSwapPair1[j] == mAnswerSwapPair2[i]){
                        if (mSelectedSwapPair2[j] == mAnswerSwapPair1[i]) {
                            isAnswerPresent[i] = true;
                        }
                    }
                }else {
                    isAnswerPresent[i] = false;
                    return isAnswerPresent;
                }
            }
        }

        return isAnswerPresent;
    }

}

