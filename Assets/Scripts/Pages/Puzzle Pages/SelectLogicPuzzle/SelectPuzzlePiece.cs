﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectPuzzlePiece : MonoBehaviour {

    public OpenAdditionalInformationPanel mAdditionalInfoPanel;
    public SelectPiecePuzzle mPuzzleScript;

    private Color mDefaultColor;
    private bool mIsSelected = false;

    void Start() {
        mDefaultColor = this.GetComponent<Image>().color;
    }

    public void SelectPiece() {  

        if (mIsSelected) {
            mIsSelected = false;
            mPuzzleScript.Deselect(this);
        }else {
            mAdditionalInfoPanel.OpenPanel();
            
            Color tempColor = mPuzzleScript.Select(this);

            if (tempColor != Color.white) {
                mIsSelected = true;
                this.GetComponent<Image>().color = tempColor;
            }else {
                mPuzzleScript.Deselect(this);
            }
        }
    }

    public void DeselectPiece() {
        mIsSelected = false;
        this.GetComponent<Image>().color = mDefaultColor;
        mAdditionalInfoPanel.OpenPanel();
    }
}
