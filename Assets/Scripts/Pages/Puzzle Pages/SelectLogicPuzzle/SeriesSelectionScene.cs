﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeriesSelectionScene : SelectPiecePuzzle {

    public WolfStorySelectionPuzzle mStorySelectionPuzzle;
    public AdditionalInformationPanel mPanel;

    public List<string> mStartText;
    public List<string> mFailureText;

    public new bool CheckAnswers() {
       
        List<bool> isAnswerPresent = new List<bool>(mRequiredSelections);
        for(int i = 0; i < mRequiredSelections; i++) {
            isAnswerPresent.Add(false);
        }

        for (int j = 0; j < mSelectedPieces.Count; j++) {            
            for (int i = 0; i < mPuzzlePieceAnswers.Count; i++) {
                if (mSelectedPieces[j] == mPuzzlePieceAnswers[i]) {
                    isAnswerPresent[j] = true;
                }
            }
        }

        bool areAllAnswersPresent = true;

        for (int i = 0; i < isAnswerPresent.Count; i++) {
            if (!isAnswerPresent[i]) {
                areAllAnswersPresent = false;
            }
        }

        if (areAllAnswersPresent) {
            return true;
        }
        else {
            mPanel.UpdatePanel(mFailureText);
            return false;
        }
        
    }
    
    public void OpenSceneWithPreloadedSelections(List<SelectPuzzlePiece> selections) {

        mRequiredSelections++;

        for(int i = 0; i < selections.Count; i++) {
            for(int j = 0; j < mPuzzlePieces.Count; j++) {
                if(selections[i] == mPuzzlePieces[j]) {
                    mPuzzlePieces[j].SelectPiece();
                }
            }
        }

        mRequiredSelections--;

        mPanel.UpdatePanel(mStartText);

    }
}
