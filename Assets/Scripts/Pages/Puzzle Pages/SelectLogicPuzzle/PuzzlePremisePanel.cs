﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzlePremisePanel : MonoBehaviour {

    public GameObject mSideOpenButton;

	public void OpenOrClosePanel() {
        this.gameObject.SetActive(!this.gameObject.activeSelf);
        mSideOpenButton.SetActive(!this.gameObject.activeSelf);           
        
    }
}
