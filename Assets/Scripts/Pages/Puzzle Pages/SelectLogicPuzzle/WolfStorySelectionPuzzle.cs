﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Few things to give better feedback on this puzzle
//Tell different responses if too many or too few deer are selected

public class WolfStorySelectionPuzzle : PuzzlePage {

    public List<SeriesSelectionScene> mPuzzleScenes;
    public List<string> mInfoTextAtEachSceneStart;

    private List<SelectPuzzlePiece> mSelections;
    private int mCurrentSceneInt = 0;

    public void NextScene() {
        bool wasCorrect = mPuzzleScenes[mCurrentSceneInt].CheckAnswers();

        if (wasCorrect) {
            mCurrentSceneInt++;
            if (mCurrentSceneInt < mPuzzleScenes.Count) {
                OpenNextScene(mCurrentSceneInt);
            }else {
                NextPage();
            }
        } 
    }

    private void OpenNextScene(int sceneToOpen) {
        mPuzzleScenes[sceneToOpen - 1].gameObject.SetActive(false);

        //Update the startingSelectionsFirst
        if(sceneToOpen < mPuzzleScenes.Count) {

            List<SelectPuzzlePiece> selections = mPuzzleScenes[sceneToOpen - 1].GetSelectedPieces();
            mPuzzleScenes[sceneToOpen - 1].ClearSelectedPieces();
            mPuzzleScenes[sceneToOpen].gameObject.SetActive(true);
            mPuzzleScenes[sceneToOpen].OpenSceneWithPreloadedSelections(selections);

        }
        else {
            NextPage();
        }
    }
}
