﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationMessage : Message {

    public AnimationPage mAnimationPage;
    public GameObject mNextImage;
    public float mDelaySkipSeconds = 2.5f;

    private float mTimeSinceOpened = 0f;

    protected override void FinishMessageChain() {

        mNextImage.SetActive(false);

        if (mNextMessage != null) {
            mNextMessage.OpenMessageFunction();
            this.gameObject.SetActive(false);
        }
        else if (mAnimationPage != null) {
            mAnimationPage.CompletedScene();
        }
    }

    void Update() {
        mTimeSinceOpened += Time.deltaTime;

        if(mTimeSinceOpened > mDelaySkipSeconds) {
            if (!mNextImage.activeSelf) {
                mNextImage.SetActive(true);
            }
        }

    }

    public void NextMessageManual() {
        if (mTimeSinceOpened >= mDelaySkipSeconds) {
            StopCoroutine(OpenTextMessage());
            FinishMessageChain();
        }        
    }
}
