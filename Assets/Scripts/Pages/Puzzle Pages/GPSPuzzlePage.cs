﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GPSPuzzlePage : PuzzlePage {

    public GameObject mGPSDisplay;
    public Text mGPSStatusText;
    public Text mGPSDebugCheck;
    public GPSPoint mSolutionGPS;

    public string mFailureText = "Unable to establish connection.  Make sure you are close enough.";
    public string mConnectingText = "Connecting...";

    private GPSManager mGPSManager;

    private int mGPSFailureCount = 0;
    private int mGPSFailureLimit = 3;


    public void PressedHereButton() {

        if(mGPSManager == null) {
            mGPSManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GPSManager>();
        }



        mGPSManager.UpdateGPSLocationManually();

        StartCoroutine(CheckingGPS());

        mGPSFailureCount++;

        if(mGPSFailureCount >= mGPSFailureLimit) {
            mGPSManager.StopAllCoroutines();
            GPSManager.mGPSIsPaused = false;
            GPSManager.mPlayerGPSPoint = null;
            mGPSManager.gameObject.SetActive(false);
            mGPSManager.gameObject.SetActive(true);
            mGPSFailureCount = 0;
            
            
        }

    }

    IEnumerator CheckingGPS() {

        yield return new WaitForSeconds(0.1f);

        mGPSDisplay.SetActive(true);

#if UNITY_EDITOR
        mGPSStatusText.text = "Skipping";
        yield return new WaitForSeconds(0.5f);
        RequirementMet();
#endif

        if (Input.location.isEnabledByUser) {            
            mGPSStatusText.text = mConnectingText;
        }else {
            mGPSStatusText.text = "GPS Not Enabled by user.  Please enable it to continue";
            //this should exit the coroutine
            yield break;
        }

        yield return new WaitForSeconds(0.6f);

        if(mGPSDebugCheck != null) {
            mGPSDebugCheck.text = GPSManager.mPlayerGPSPoint.mLatitude.ToString() + " , " + GPSManager.mPlayerGPSPoint.mLongitude.ToString();
        }  
        

        //It crashes here for some Reason<------------------------------------
        if (mSolutionGPS.AreGPSPointsNear()) {
            mGPSStatusText.text = "Success";
            yield return new WaitForSeconds(0.5f);
            RequirementMet();
        }
        else {
            mGPSStatusText.text = mFailureText;
            yield return new WaitForSeconds(2f);
            if (mGPSDisplay.activeSelf) {
                mGPSDisplay.SetActive(false);
            }
        }

    }
}
