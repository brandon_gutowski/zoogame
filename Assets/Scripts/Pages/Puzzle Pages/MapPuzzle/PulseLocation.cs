﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PulseLocation : MonoBehaviour {

    public GameObject mPulseObject;
    public List<HiddenObject> mHidingObjects;
    public MapPuzzlePage mPuzzlePage;

    public float mPulseWaitTime = 1.0f;

    private int mPulseDivisions = 4;
    private float mPulseTravelTime = 1f;

    void Start() {
        //PulseTest();
    }

    public void Pulse() {
        StartCoroutine(PulseCoroutine());
    }

    IEnumerator PulseCoroutine() {
        mPulseObject.SetActive(true);
        yield return new WaitForSeconds(mPulseWaitTime / 2f);

        Rect pulseRect = mPulseObject.GetComponent<RectTransform>().rect;

        //Want to imitate a pulse travelling 
        for (int j = 1; j <= mPulseDivisions; j++) {
            for (int i = 0; i < mHidingObjects.Count; i++) {
                mHidingObjects[i].CheckIfRevealledByPulse(mPulseObject.transform, (mPulseObject.GetComponent<RectTransform>().rect.width/2f) / mPulseDivisions * j);
                //Debug.Log("PulseWidth: " + ((mPulseObject.GetComponent<RectTransform>().rect.width / 2f) / (mPulseDivisions * j)) + " , Transform: " + mPulseObject.transform.position.x);
            }
            yield return new WaitForSeconds(mPulseTravelTime / mPulseDivisions);
        }

        yield return new WaitForSeconds(mPulseWaitTime / 2f);
        mPulseObject.SetActive(false);

    }

    //For this to function MapView must be on the same gaemObject
    /*public void TestPulseFunction() {

        StartCoroutine(PulseTest());
    }

    IEnumerator PulseTest() {
        Debug.Log("Start");

        Pulse();

        GPSPoint spotNearFirstHidden = new GPSPoint(42.477434, -83.161036);
        GPSPoint spotNearSecondHidden = new GPSPoint(42.476729, -83.160217);

        MapView mapView = this.GetComponent<MapView>();        

        yield return new WaitForSeconds(2f);

        mapView.UpdatePlayerLocation(spotNearFirstHidden);
        Pulse();

        yield return new WaitForSeconds(2f);

        mapView.UpdatePlayerLocation(spotNearSecondHidden);
        Pulse();

        yield return new WaitForSeconds(2f);

        Debug.Log("EndTest");

    }*/
}
