﻿using UnityEngine;
using System.Collections.Generic;

public class PinLocation : MonoBehaviour {

    public GameObject mPlayerIcon;
    public GameObject mMap;
    public MapPuzzlePage mPuzzlePage;

    public GameObject mPinPrefab;

    [HideInInspector]
    public List<GPSPoint> mGPSPoints = new List<GPSPoint>();

    private int mCurrentPins = 0;

    public void PinCurrent() {

        GameObject pin = Instantiate(mPinPrefab, mPlayerIcon.transform.position, mPlayerIcon.transform.rotation, mMap.transform) as GameObject;
        pin.transform.SetSiblingIndex(pin.transform.GetSiblingIndex() - 1);
        pin.SetActive(true);

        if(mPuzzlePage.mRequiredPinObjects >= mCurrentPins){
            mCurrentPins++;
            mGPSPoints.Add(GPSManager.mPlayerGPSPoint);
            mPuzzlePage.AddPinObject();
        }
        
    }
}
