﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolvePuzzleButton : MonoBehaviour {

    public MapPuzzlePage mMapPuzzle;
    
    public void SubmitAnswer() {

        mMapPuzzle.CheckPuzzle(true);
    }
}
