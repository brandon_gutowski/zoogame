﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HiddenObject : MonoBehaviour {

    [HideInInspector]
    public bool mMarked = false;

    private bool mRevealed = false;

	public void CheckIfRevealledByPulse(Transform playerTrans, float pulseWidth) {

        if (!mRevealed) {

            Vector2 playerPosition = new Vector2(playerTrans.parent.localPosition.x, playerTrans.parent.localPosition.y);
            Vector2 thisPosition = new Vector2(transform.localPosition.x, transform.localPosition.y);

            float distance = Vector2.Distance(playerPosition, thisPosition);

            if (distance <= pulseWidth) {
                
                Reveal();
            }
        }
    }

    private void Reveal() {
        this.GetComponent<Image>().enabled = true;
        mRevealed = true;
    }

    public void MarkObjectLocation() {
        mMarked = true;
    }
}
