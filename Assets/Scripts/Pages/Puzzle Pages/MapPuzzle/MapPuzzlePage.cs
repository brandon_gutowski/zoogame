﻿using UnityEngine;
using System.Collections.Generic;

public class MapPuzzlePage: PuzzlePage {

    public List<HiddenObject> mRequiredObjectsToReveal;
    public int mRequiredPinObjects;

    private int mCurrentPinObjects = 0;
    
    public void CheckIfMarked() {

        bool allMarked = true;

        for(int i = 0; i < mRequiredObjectsToReveal.Count; i++) {
            if (!mRequiredObjectsToReveal[i].mMarked) {
                allMarked = false;
                break;
            }
        }

        if(mCurrentPinObjects != mRequiredPinObjects) {
            allMarked = false;
        }

        CheckPuzzle(allMarked);
    }

    public void AddPinObject() {
        mCurrentPinObjects++;
    }
}
