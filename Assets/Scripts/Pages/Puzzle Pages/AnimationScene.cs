﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationScene : MonoBehaviour {

    public AnimationMessage mFirstMessage;

    public void StartAnimationText() {
        mFirstMessage.OpenMessageFunction();
    }
}
