﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipePuzzleGrid : MonoBehaviour {

    [HideInInspector]
    public List<PipePuzzleSlot> mPipeSlots = new List<PipePuzzleSlot>();//Slot 0 is in the upper left.  Slots go left to right, from top to bottom
    public int mColumns;
    public int mRows;
    public Vector2 mPuzzleSlotSize;
    public Vector3 mFirstSlotPosition;
    public List<int> mRequiredPipeSlots;

    //Check all Children of this gameobject and add their slot if they are    

    void Awake() {
        PopulatePipeSlots();
    }

    public void DropIntoSlot(Pipe pipe, int slotInt) {

        if (mPipeSlots[slotInt].CanDropIntoSlot()) {
            pipe.transform.position = mPipeSlots[slotInt].mLocalPosition;
            mPipeSlots[slotInt].mCurrentPipeInSlot = pipe;
            MovePuzzlePieceFromOldSlot(pipe.mCurrentSlot);
        }
        else {
            ReturnToStartingSlot(pipe);
        }
    }

    public int FindNearestSlot(Vector3 draggablePosition) {

        int nearestObject = -1;
        float nearestDistance = 10000;

        for (int i = 0; i < mPipeSlots.Count; i++) {

            float distance = Vector3.Distance(mPipeSlots[i].mLocalPosition, draggablePosition);

            if (distance < nearestDistance) {
                nearestDistance = distance;
                nearestObject = i;
            }
        }
        return nearestObject;
    }

    public void MovePuzzlePieceFromOldSlot(PipePuzzleSlot previousSlot) {

        for (int i = 0; i < mPipeSlots.Count; i++) {
            if (mPipeSlots[i] == previousSlot) {
                mPipeSlots[i].mCurrentPipeInSlot = null;
            }
        }
    }

    public void ReturnToStartingSlot(Pipe pipe) {
        pipe.ReturnToStartPosition();
    }

    public void PopulatePipeSlots() {

        for(int rows = 0; rows <= mRows; rows++) {
            for(int columns = 0; columns <= mColumns; columns++) {
                mPipeSlots.Add(CreateNewPipeSlot(rows, columns));
            }
        }
    }

    private PipePuzzleSlot CreateNewPipeSlot(int row, int column) {

        Vector3 position = mFirstSlotPosition;

        position.x += column * mPuzzleSlotSize.x;
        position.y += row * mPuzzleSlotSize.y;

        PipePuzzleSlot slot = new PipePuzzleSlot(position);

        return slot;
    }
}
