﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipePuzzle : MonoBehaviour {
    
    public PipePuzzleGrid mGridSpace;
    public GameObject mPipeSpawner;

    public float mTimeToFillOnePipe;
    public int mNumOfPipesUntilStart;

    public List<Pipe> mStartingPipeSpawnRun;
    public List<Pipe> mPipeSpawningPattern;
}
