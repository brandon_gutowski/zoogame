﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pipe : MonoBehaviour {

    public PipePuzzleGrid mPuzzleGrid;
    public PipePuzzleSlot mCurrentSlot;
    public List<bool> mIsSideOpen; //Top is 0, proceed clockwise
    [HideInInspector]
    public bool mIsFilled = false;
    public bool mIsInSpawningSlot = false;

    protected int mSnapDistance = 100;


    void Awake() {
        
       Debug.Assert(mIsSideOpen.Count == 4);
       Debug.Assert(AreOnlyTwoSidesOpen());
        
        
    }

    private bool AreOnlyTwoSidesOpen() {
               
        int numberOfSideOpen = 0;

        for (int i = 0; i < mIsSideOpen.Count; i++) {
            if(mIsSideOpen[i] == true) {
                numberOfSideOpen++;
            }
        }

        if(numberOfSideOpen == 2) {
            return true;
        }
        else {
            return false;
        }          
    }

    public virtual void BeginDrag() {
        this.transform.SetParent(mPuzzleGrid.transform.parent, true);
 
    }

    public void Drag() {

        Vector3 position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        position.z = 0;

        this.transform.position = position;
    }

    public virtual void Drop() {

        this.transform.SetParent(mPuzzleGrid.transform, true);

        int nearestSlotInt = mPuzzleGrid.FindNearestSlot(this.transform.position);
        float distanceStarter = Vector3.Distance(mPuzzleGrid.mPipeSlots[nearestSlotInt].mLocalPosition, this.transform.position);


        if (distanceStarter < mSnapDistance) {
            mPuzzleGrid.DropIntoSlot(this, nearestSlotInt);
        }
        else {
            ReturnToStartPosition();
        }
    }

    public void ReturnToStartPosition() {
        if (mIsInSpawningSlot) {
            //Place in nearest open slot?  Maybe just return to slot.
        }else {
            transform.position = mCurrentSlot.mLocalPosition;
        }
    }

}
