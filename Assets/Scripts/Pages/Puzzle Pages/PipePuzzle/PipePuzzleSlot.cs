﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipePuzzleSlot{

    public Pipe mCurrentPipeInSlot;
    public Vector3 mLocalPosition;
    

    public PipePuzzleSlot(Vector3 position) {
        mLocalPosition = position;
    }

    public bool CanDropIntoSlot() {
        if(mCurrentPipeInSlot != null) {            
            return false;            
        }

        return true;        
    }

}
