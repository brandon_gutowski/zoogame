﻿using UnityEngine;
using System.Collections;

public class PuzzlePage : BasePage {

    protected void RequirementMet()
    {
        //Event for when a requirement is met

        mNextPage.OpenPage(this);
    }

    public void CheckPuzzle(bool isPuzzleSolved) {
        if (isPuzzleSolved) {
            RequirementMet();
        }
    }

}
