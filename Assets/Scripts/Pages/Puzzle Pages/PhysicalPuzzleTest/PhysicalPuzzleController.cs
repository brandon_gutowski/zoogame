﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class PhysicalPuzzleController : MonoBehaviour {

	public enum Shape {
        T,
        O,
        X
    }

    public Text mShapeDialText;
    public ColorDial mColorDialImage;

    public Shape mCurrentShape;
    public Color mCurrentColor;

    public List<ControllerButton> mButtons;

    public delegate void StateUpdated(PhysicalPuzzleController.Shape shape, Color color);
    public static event StateUpdated OnStateUpdate;
    public static void CallStateUpdated(PhysicalPuzzleController.Shape shape, Color color) {
        if (OnStateUpdate != null) {
            OnStateUpdate(shape, color);
        }
    }

    void Start() {

        CallStateUpdated(mCurrentShape, mCurrentColor);
    }

    public void TurnShapeDial() {

        switch (mCurrentShape) {

            case Shape.T:
                mCurrentShape = Shape.O;
                break;

            case Shape.O:
                mCurrentShape = Shape.X;
                break;

            case Shape.X:
                mCurrentShape = Shape.T;
                break;

        }       

        UpdateShape(mCurrentShape);
    }

    public void UpdateColor(Color newColor) {
        mCurrentColor = newColor;
        CallStateUpdated(mCurrentShape, mCurrentColor);
    }

    private void UpdateShape(Shape newShape) {
        mShapeDialText.text = newShape.ToString();
        CallStateUpdated(mCurrentShape, mCurrentColor);
    }

    public void ButtonPressed(ControllerButton buttonScript) {

        Shape shape = buttonScript.GetShape();
        Color color = buttonScript.GetActivatedColor();

        if(shape == mCurrentShape && color == mCurrentColor) {
            buttonScript.ActivateButton();
        }

        CheckIfAllButtonsAreActivated();
    }

    private void CheckIfAllButtonsAreActivated() {

        bool allButtonsActivated = true;

        for(int i = 0; i < mButtons.Count; i++) {
            if (!mButtons[i].IsButtonActivated()) {
                allButtonsActivated = false;
            }
        }

        if (allButtonsActivated) {
            this.GetComponent<PuzzlePage>().CheckPuzzle(true);
        }
    }



}
