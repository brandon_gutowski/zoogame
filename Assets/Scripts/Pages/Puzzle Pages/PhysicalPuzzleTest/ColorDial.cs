﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ColorDial : MonoBehaviour {

    public PhysicalPuzzleController mController;
    public List<Color> mColorSet1;
    public List<Color> mColorSet2;

    public List<Image> mImages;//Need to be in the order of the 2 colorSets.  Colors in the 0 slot will go into this 0 slot
    
    private bool mUseColorSet1 = true;
    private int mDialLocation = 0;

    public void ToggleSwitch() {
        mUseColorSet1 = !mUseColorSet1;

        UpdateImages();
        UpdateColor();
    }

    public void PressColorButton() {

        if(mDialLocation > 1) {
            mDialLocation = 0;
        }
        else {
            mDialLocation++;
        }

        UpdateColor();
    }

    private void UpdateImages() {

        for(int i = 0; i < mImages.Count; i++) {
            if (mUseColorSet1) {
                mImages[i].color = mColorSet1[i];
            }
            else {
                mImages[i].color = mColorSet2[i];
            }
        }
    }

    private void UpdateColor() {

        Color currentColor;

        if (mUseColorSet1) {
            currentColor  = mColorSet1[mDialLocation];
        }
        else {
            currentColor = mColorSet2[mDialLocation];
        }

        mController.UpdateColor(currentColor);
        this.GetComponent<Image>().color = currentColor;
    }
}
