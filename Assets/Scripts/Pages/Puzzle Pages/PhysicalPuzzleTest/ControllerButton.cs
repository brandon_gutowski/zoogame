﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ControllerButton : MonoBehaviour {

    private bool mIsActivated;

    public Color mActivatedColor;
    public PhysicalPuzzleController.Shape mShape;
    public Color mRestingColor;

    public Color GetActivatedColor() {
        return mActivatedColor;
    }

    public PhysicalPuzzleController.Shape GetShape() {
        return mShape;
    }

    public bool IsButtonActivated() {
        return mIsActivated;
    }

    void Awake() {
        PhysicalPuzzleController.OnStateUpdate += UpdateColor;
    }

    void OnDestroy() {
        PhysicalPuzzleController.OnStateUpdate -= UpdateColor;
    }

    public void ActivateButton() {
        this.GetComponent<Image>().color = mActivatedColor;
        this.GetComponent<Button>().enabled = false;
        mIsActivated = true;
    }

    public void UpdateColor(PhysicalPuzzleController.Shape shape, Color color) {
        if (!mIsActivated) {
            if(shape == mShape && color == mActivatedColor) {
                this.GetComponent<Image>().color = mActivatedColor;
            }
            else {
                this.GetComponent<Image>().color = mRestingColor;
            }
        }
    }
}
