﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PicturePasswordPuzzle : PuzzlePage {

    public List<InputField> mAnswerBoxes;
    public string mAnswer; //Answer MUST be the same length as the mAnswerBoxes.Count.  Answer should always be lower case

    void Start() {
        if(mAnswer.Length != mAnswerBoxes.Count) {
            Debug.LogError("mAnswer and mAnswerBoxes are not the same length.  This puzzle cannot be solved.");
        }
    }

    public void LetterEntered() {

        if (AreAllBoxesFilled()) {
            if (CheckEachBox()) {
                RequirementMet();
            }
        }

    }

    private bool AreAllBoxesFilled() {

        for (int i = 0; i < mAnswerBoxes.Count; i++) {
            if (mAnswerBoxes[i].textComponent.text == "") {
                return false;
            }
        }

        return true;
    }

    private bool CheckEachBox() {

        bool allAreCorrect = true;

        for(int i = 0; i < mAnswerBoxes.Count; i++) {
            if(mAnswerBoxes[i].text.ToLower()[0] != mAnswer[i]) {
                allAreCorrect = false;
                mAnswerBoxes[i].textComponent.color = Color.red;
            }
            else {
                mAnswerBoxes[i].textComponent.color = Color.black;
            }
        }

        return allAreCorrect;

    }
}
