﻿using UnityEngine;
using System.Collections;

public class SubmitAnswersButton : MonoBehaviour {

    public PuzzlePage mPuzzlePage;
    public SlotStorage mPuzzleAnswerCheck;

    public void AnswerCheck() {
       mPuzzlePage.CheckPuzzle(mPuzzleAnswerCheck.CheckAnswers());
    }

}
