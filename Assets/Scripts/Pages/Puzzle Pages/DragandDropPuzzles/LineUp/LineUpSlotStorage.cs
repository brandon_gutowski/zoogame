﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LineUpSlotStorage : SwappableSlotStorage {

    //public delegate bool CheckAnswersDel();

	//Always put the answers in the first half of the slots for the Lineup Puzzle mSlots

    //Could give each draggable a number indicating the order of their answers, then check if the y position of each is above the smaller numbers than the draggable's number
    //Need to make a Slots Class that stores whether the slot is full. I can also have a coordinated answer int on the slot and then the match answers script will just compare the two
    //I can also do highlight and animation effects later
    
   

    public override bool CheckAnswers() {

        bool allAnswersCorrect = true;

        for(int i = 0; i < (mSlots.Count/2); i++) {
            if (!mSlots[i].HasCorrectAnswer()) {
                allAnswersCorrect = false;
            }
        }

        return allAnswersCorrect;
    }
}
