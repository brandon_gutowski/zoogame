﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LineUpPuzzleDraggable : PuzzleDraggable {

    public LineUpSlotStorage mLineUpSlotStorage;
    public UpdateAddtionalInfoPanel mAdditionalInformationPanel;

    protected override void AddtionalAwakeFunctions() {
        mLineUpSlotStorage.SetStarterSpotAsFilled(this);
    }

}
