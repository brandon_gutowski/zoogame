﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PuzzleSlot : MonoBehaviour {

    [HideInInspector]
    public PuzzleDraggable mPuzzlePieceInSlot = null;

    [SerializeField]
    protected int mRequiredAnswer = -1;

   public int GetRequiredAnswer() {
        return mRequiredAnswer;
    }

    public virtual bool HasCorrectAnswer() {

        if (mRequiredAnswer > -1) {
            if (mPuzzlePieceInSlot != null) {
                if (mPuzzlePieceInSlot.mAnswer == mRequiredAnswer) {
                    return true;
                }
                
            }
        }

        return false;
    }



}
