﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HabitatPuzzleDraggable : PuzzleDraggable {

    public delegate void SomethingSelected(string name);
    public static event SomethingSelected Selected;
    public static void OnSelect(string name) {
        if(Selected != null) {
            Selected.Invoke(name);
        }
    }

    public Image mHabitatArea;

    private float mDeselectedAlphaInt = 50f/255f;
    private Vector3 mHomePosition;

    void Start() {
        mHomePosition = mStartPosition;
        mSlotStorage.SetStarterSpotAsFilled(this);
        Selected += Deselect;
    }

    void OnDestroy() {
        Selected -= Deselect;
    }

    public void Select() {
        OnSelect(this.name);
    }

    public void Deselect(string name) {
        if (name != this.name) {
            ChangeColorAlpha(mDeselectedAlphaInt);
        }else {
            ChangeColorAlpha(1.0f);
        }
    }

    private void ChangeColorAlpha(float value) {
        if (mHabitatArea != null) {
            Color tempColor = mHabitatArea.color;
            tempColor.a = value;

            mHabitatArea.color = tempColor;
        }
    }


    public override void Drop() {

        this.transform.SetParent(mSlotStorage.transform, true);

        int nearestSlotInt = mSlotStorage.FindNearestSlot(this.transform.position);
        float distanceStarter = Vector3.Distance(mSlotStorage.mSlots[nearestSlotInt].transform.position, this.transform.position);

        if (mSlotStorage.mSlots[nearestSlotInt].GetRequiredAnswer() > -1) {
            if (distanceStarter < mSnapDistance) {
                mSlotStorage.DropIntoSlot(this, nearestSlotInt);
            }
            else {
                ReturnToHomeSlot();
            }
        }
        else {
            ReturnToHomeSlot();
        }
    }

    private void ReturnToHomeSlot() {

        if(mStartPosition != mHomePosition) {
            mSlotStorage.MovePuzzlePieceFromOldSlot(mStartPosition);
        }
        this.transform.position = mHomePosition;
        mStartPosition = mHomePosition;
    }
}
