﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HabitatPuzzleSlot : AnswerPuzzleSlot {

    public Image mHabitatArea;

    private Color mStartingColor;
    private Color mLitColor;

    private void Start() {
        mStartingColor = mHabitatArea.color;
        mLitColor = mHabitatArea.color;

        mLitColor.a = 1f;
    }

    public void HighlightHabitatImage() {
        mHabitatArea.color = mLitColor;
    }

    public void DarkenHabitatImage() {
        mHabitatArea.color = mStartingColor;
    }
}
