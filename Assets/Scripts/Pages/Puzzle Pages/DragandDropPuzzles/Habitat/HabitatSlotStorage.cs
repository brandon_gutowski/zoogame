﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HabitatSlotStorage : SwappableSlotStorage {

	public override void AdditionalDropInFunctions(PuzzleDraggable draggable, int slotInt) {

        for(int i = 0; i < mSlots.Count; i++) {            

            HabitatPuzzleSlot slot = mSlots[i] as HabitatPuzzleSlot;

            if(slot != null) {
                if (mSlots[i].mPuzzlePieceInSlot == draggable) {
                    slot.HighlightHabitatImage();
                }
                else {
                    slot.DarkenHabitatImage();
                }
            }

        }

    }
}
