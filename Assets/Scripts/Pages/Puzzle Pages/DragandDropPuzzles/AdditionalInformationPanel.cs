﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class AdditionalInformationPanel : MonoBehaviour {

    public List<Image> mImageToUpdate;
    public List<Text> mTextToUpdate;

    //Lists must be in the same order assigned as the ones sent in the function
    public void UpdatePanel(List<string> texts = null, List<Sprite> images = null) {

        if (mImageToUpdate.Count > 0) {
            Debug.Assert(mImageToUpdate.Count == images.Count);
            
            for(int i = 0; i < images.Count; i++) {
                if(images[i] != null) {
                    mImageToUpdate[i].enabled = true;
                    mImageToUpdate[i].sprite = images[i];
                }else {
                    mImageToUpdate[i].enabled = false;
                }
               
            }
        }

        if(mTextToUpdate.Count > 0) {
            Debug.Assert(mTextToUpdate.Count == texts.Count);

            for(int i = 0; i < texts.Count; i++) {
                mTextToUpdate[i].text = texts[i];
            }
        }

        MoreUpdates();
    }

    protected virtual void MoreUpdates() {

    }
}
