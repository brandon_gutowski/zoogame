﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenAdditionalInformationPanel : MonoBehaviour {

    public GameObject mAdditionalInfoPanel;
    [TextArea(1, 5)]
    public List<string> mInfoTexts;
    public List<Sprite> mInfoSprites;

    public void OpenPanel() {
        mAdditionalInfoPanel.GetComponent<AdditionalInformationPanel>().UpdatePanel(mInfoTexts, mInfoSprites);
        OpenOrClosePanel panel = mAdditionalInfoPanel.GetComponent<OpenOrClosePanel>();
        if(panel != null) {
            panel.Open();
        }        
    }
 
}
