﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnswerPuzzleSlot : PuzzleSlot {

    protected Color mWrongAnswerColor = Color.red;
    protected Color mCorrectAnswerColor;
    [HideInInspector]

    public override bool HasCorrectAnswer() {

        if (mRequiredAnswer > -1) {
            if (mPuzzlePieceInSlot != null) {
                if (mPuzzlePieceInSlot.mAnswer == mRequiredAnswer) {
                    this.GetComponent<Image>().color = mCorrectAnswerColor;
                    return true;
                }
                this.GetComponent<Image>().color = mWrongAnswerColor;
            }
        }

        return false;
    }

    void Start() {
        mCorrectAnswerColor = this.GetComponent<Image>().color;
    }

    public void WrongAnswer() {
        this.GetComponent<Image>().color = mWrongAnswerColor;
    }

    public void CorrectAnswer() {
        this.GetComponent<Image>().color = mCorrectAnswerColor;
    }
}
