﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwappableSlotStorage : SlotStorage {

   public override void DropIntoSlot(PuzzleDraggable draggable, int slotInt) {

        if (mSlots[slotInt].mPuzzlePieceInSlot == null) {
            draggable.transform.position = mSlots[slotInt].transform.position;
            mSlots[slotInt].mPuzzlePieceInSlot = draggable;
            MovePuzzlePieceFromOldSlot(draggable.MStartPosition);
        }
        else {
            //Debug.Log(slotInt + " slot, " + mSlots[slotInt].mPuzzlePieceInSlot.name + " : " + draggable.name);
            SwapPuzzlePieceSlots(draggable, slotInt);
        }

        AdditionalDropInFunctions(draggable, slotInt);
    }

    public virtual void AdditionalDropInFunctions(PuzzleDraggable draggable, int slotInt) {

    }


    //Slot Int is where you want to move the piece
    protected void SwapPuzzlePieceSlots(PuzzleDraggable draggableToMove, int slotInt) {

        Vector3 previousPosition = draggableToMove.MStartPosition;
        Vector3 futurePosition = mSlots[slotInt].transform.position;
        int previousSlotInt = -1;

        for (int i = 0; i < mSlots.Count; i++) {

            if (mSlots[i].mPuzzlePieceInSlot == draggableToMove) {
                previousSlotInt = i;
                break;
            }
        }

        Debug.Assert(previousSlotInt != -1);

        draggableToMove.gameObject.transform.position = futurePosition;
        mSlots[slotInt].mPuzzlePieceInSlot.transform.position = previousPosition;

        mSlots[previousSlotInt].mPuzzlePieceInSlot = mSlots[slotInt].mPuzzlePieceInSlot;
        mSlots[slotInt].mPuzzlePieceInSlot = draggableToMove;
    }

}
