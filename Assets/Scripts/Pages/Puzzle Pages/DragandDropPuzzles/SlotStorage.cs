﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlotStorage : MonoBehaviour {

    public List<PuzzleSlot> mSlots; //Always put the answers in the first half of the slots

    public virtual bool CheckAnswers() {

        foreach (PuzzleSlot p in mSlots) {
            AnswerPuzzleSlot puzzleSlot = p as AnswerPuzzleSlot;

            if (puzzleSlot != null) {
                if (puzzleSlot.mPuzzlePieceInSlot != null) {
                    if (p.mPuzzlePieceInSlot.mAnswer != -1) {
                    
                    
                        if (!puzzleSlot.HasCorrectAnswer()) {
                            Debug.Log(p.name);
                            return false;
                        }
                    }
                }
                else {
                    Debug.Log(p.name + " : empty");
                    return false;
                }
            }
        }

        return true;
    }

    public void SetStarterSpotAsFilled(PuzzleDraggable draggable) {

        for (int i = 0; i < mSlots.Count; i++) {
            if (mSlots[i].transform.position == draggable.MStartPosition) {
                mSlots[i].mPuzzlePieceInSlot = draggable;
                break;
            }
        }
    }

    public virtual void DropIntoSlot(PuzzleDraggable draggable, int slotInt) {

        if (mSlots[slotInt].mPuzzlePieceInSlot == null) {
            draggable.transform.position = mSlots[slotInt].transform.position;
            mSlots[slotInt].mPuzzlePieceInSlot = draggable;
            MovePuzzlePieceFromOldSlot(draggable.MStartPosition);
        }
    }

    public int FindNearestSlot(Vector3 draggablePosition) {

        int nearestObject = -1;
        float nearestDistance = 10000;

        for (int i = 0; i < mSlots.Count; i++) {

            float distance = Vector3.Distance(mSlots[i].transform.position, draggablePosition);

            if (distance < nearestDistance) {
                nearestDistance = distance;
                nearestObject = i;
            }
        }
        return nearestObject;
    }

    public void MovePuzzlePieceFromOldSlot(Vector3 previousPosition) {

        for (int i = 0; i < mSlots.Count; i++) {
            if (mSlots[i].transform.position == previousPosition) {
                mSlots[i].mPuzzlePieceInSlot = null;
            }
        }
    }

}
