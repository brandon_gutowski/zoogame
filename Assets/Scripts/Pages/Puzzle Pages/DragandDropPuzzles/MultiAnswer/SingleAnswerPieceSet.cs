﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleAnswerPieceSet : SwappableSlotStorage {

    private List<int> mAnswersRequired = new List<int>();

    void Awake() {
        DetermineRequiredAnswers();
    }

    public override bool CheckAnswers() {

        for(int i = 0; i < mSlots.Count; i++) {
            if (mSlots[i].mPuzzlePieceInSlot == null) return false;
        }
        for (int j = 0; j < mSlots.Count; j++) {
            
            bool exsitsInSlot = false;

            AnswerPuzzleSlot slot = mSlots[j] as AnswerPuzzleSlot;

            for (int i = 0; i < mAnswersRequired.Count; i++) {
                if (mAnswersRequired[i] == mSlots[j].mPuzzlePieceInSlot.mAnswer) {
                    exsitsInSlot = true;

                    if(slot != null) {
                        slot.CorrectAnswer();
                    }
                    
                    break;
                }
            }
            if (!exsitsInSlot) {

                if (slot != null) {
                    slot.WrongAnswer();
                }
                
                return false;
            }
        }

        //I need to highlight wrong ones at this point :(

        return true;

    }

    private void DetermineRequiredAnswers() {

        mAnswersRequired = new List<int>();

        for(int i = 0; i < mSlots.Count; i++) {
            bool isUnique = true;
            int slotAnswer = mSlots[i].GetRequiredAnswer();

            for (int j = 0; i < mAnswersRequired.Count; j++) {
                if(slotAnswer == mAnswersRequired[j]) {
                    isUnique = false;
                    break;
                }
            }

            if (isUnique) {
                mAnswersRequired.Add(slotAnswer);
            }
        }
    }
}
