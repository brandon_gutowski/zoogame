﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiAnswerPieceSet: SwappableSlotStorage {

    public List<SingleAnswerPieceSet> mPieceSets;

    public override bool CheckAnswers() {

        bool correct = true;

        for(int i = 0; i < mPieceSets.Count; i++) {
            if (!mPieceSets[i].CheckAnswers()) {
                correct = false;
                break;
            }
        }

        return correct;
    }
}
