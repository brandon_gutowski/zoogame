﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SpawnSlot : PuzzleSlot {
   
    public GameObject mDraggableToSpawn;
    public Transform mDraggableParent;
    public int mMaxAvailable;
    public Text mTextObject;

    private int mCurrentAvailable;
    private Color mDefaultColor;

    void Start() {
        mCurrentAvailable = mMaxAvailable;
        mDefaultColor = this.GetComponent<Image>().color;
    }

    public void ClickSlot() {
        if(mCurrentAvailable > 0) {

            DecreaseNumberAvailable();


            GameObject draggable = GameObject.Instantiate(mDraggableToSpawn, mDraggableParent) as GameObject;
            draggable.SetActive(true);

            draggable.GetComponent<PuzzleDraggable>().BeginDrag();
            draggable.GetComponent<PuzzleDraggable>().Drag();
        }
    }

    public void IncreaseNumberAvailable() {
        if(mCurrentAvailable < mMaxAvailable) {
            mCurrentAvailable++;
        }

        UpdateText();

        if(mCurrentAvailable > 0) {
            Enable();
        }
    }

    private void DecreaseNumberAvailable() {
        if(mCurrentAvailable > 0) {
            mCurrentAvailable--;
        }

        UpdateText();

        if(mCurrentAvailable == 0) {
            Disable();
        }
    }

    private void Disable() {
        this.gameObject.SetActive(false);
        this.GetComponent<Image>().color = Color.gray;
    }

    private void Enable() {
        this.gameObject.SetActive(true);
        this.GetComponent<Image>().color = mDefaultColor;
    }

    private void UpdateText() {
        mTextObject.text = mCurrentAvailable.ToString();
    }
}
