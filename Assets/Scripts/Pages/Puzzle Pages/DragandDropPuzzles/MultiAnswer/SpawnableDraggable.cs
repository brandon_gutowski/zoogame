﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpawnableDraggable : PuzzleDraggable {

    public Text mSpawnText;

    protected override void AddtionalAwakeFunctions() {
        mSpawnText.text = "";
    }

}
