﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnStorageSlot : SwappableSlotStorage {

    public List<SpawnSlot> mSpawnButtonsList;
    public int mNumberOfSpawningSlots; //Spawning Slots should always be the first slots

    public override void DropIntoSlot(PuzzleDraggable draggable, int slotInt) {

        if (slotInt > (mNumberOfSpawningSlots - 1)) {
            if (mSlots[slotInt].mPuzzlePieceInSlot == null) {
                draggable.transform.position = mSlots[slotInt].transform.position;
                mSlots[slotInt].mPuzzlePieceInSlot = draggable;
                MovePuzzlePieceFromOldSlot(draggable.MStartPosition);
            }
            else {
                SwapPuzzlePieceSlots(draggable, slotInt);
            }
        }
        else {//Is a Spawn Slot
            //determine which spawn slot it is and then increase its value
            int draggableId = draggable.mAnswer;
            int spawnID;
            for(int i = 0; i < mSpawnButtonsList.Count; i++) {
               spawnID = mSpawnButtonsList[i].mDraggableToSpawn.GetComponent<PuzzleDraggable>().mAnswer;
                if(spawnID == draggableId) {
                    mSpawnButtonsList[i].IncreaseNumberAvailable();
                    break;
                }
            }

            GameObject.Destroy(draggable.gameObject);
        }
    }
}
