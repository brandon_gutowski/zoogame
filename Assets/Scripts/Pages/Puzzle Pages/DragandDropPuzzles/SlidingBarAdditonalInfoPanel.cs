﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlidingBarAdditonalInfoPanel : AdditionalInformationPanel {

    public float mOpenPositionXValue;
    public GameObject mOpenButton;

    private float mClosedPositionXValue = 0.0f;
    private bool mHasBeenUpdated = false;

    void Start() {
        
    }

    protected override void MoreUpdates() {

        if (mClosedPositionXValue == 0.0f) {
            mClosedPositionXValue = mOpenButton.transform.localPosition.x;
        }

        mHasBeenUpdated = true;
        if (mOpenButton != null) {
            mOpenButton.SetActive(true);
        }
    }

    public void ToggleOpenClosed() {

        if (mHasBeenUpdated) {
            Vector3 tempPosition = mOpenButton.transform.localPosition;

            if (this.gameObject.activeSelf) {
                tempPosition.x = mClosedPositionXValue;
            }
            else {
                tempPosition.x = mOpenPositionXValue;
            }

            mOpenButton.transform.localPosition = tempPosition;

            this.gameObject.SetActive(!this.gameObject.activeSelf);
        }
    }


}
