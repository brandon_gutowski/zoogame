﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PuzzleDraggable : MonoBehaviour {
    
    public SlotStorage mSlotStorage;
    public int mAnswer; //Top of the Answer slots is always the lowest number
    
    [HideInInspector] public Vector3 MStartPosition {
        get {
            return mStartPosition;
        }
        protected set {
            mStartPosition = value;
        }
    }

    protected Color mDefaultColor;
    protected Vector3 mStartPosition;
    protected int mSnapDistance = 100;

    
    public virtual void BeginDrag() {
        mStartPosition = this.transform.position;
        this.transform.SetParent(mSlotStorage.transform.parent, true);
        ToggleDefaultColor();
    }

    public void Drag() {

        Vector3 position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        position.z = 0;

        this.transform.position = position;
    }

    public virtual void Drop() {

        this.transform.SetParent(mSlotStorage.transform, true);

        int nearestSlotInt = mSlotStorage.FindNearestSlot(this.transform.position); 
        float distanceStarter = Vector3.Distance(mSlotStorage.mSlots[nearestSlotInt].transform.position, this.transform.position);

        
        if(distanceStarter < mSnapDistance) {
            mSlotStorage.DropIntoSlot(this, nearestSlotInt);           
        }
        else {
            ReturnToStartPosition();
        }
    }

    void Awake() {
        mStartPosition = this.transform.position;

        mDefaultColor = this.GetComponent<Image>().color;

        AddtionalAwakeFunctions();
    }

    protected virtual void AddtionalAwakeFunctions() {
        mSlotStorage.SetStarterSpotAsFilled(this);

    }

    protected void ReturnToStartPosition() {
        this.transform.position = mStartPosition;
    }

    public void ToggleDefaultColor() {
        this.GetComponent<Image>().color = mDefaultColor;
    }
}
