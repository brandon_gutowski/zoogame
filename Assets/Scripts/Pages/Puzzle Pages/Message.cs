﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Message : MonoBehaviour {

    public float mTimeDelayToOpen;
    public Message mNextMessage;

    public GameObject mAnimatingObject;
    public GameObject mTextGameObject;
    public GameObject mUserIconObject;
    
    public void OpenMessageFunction() {
        this.gameObject.SetActive(true);
        StartCoroutine(OpenMessage());
    }

    public IEnumerator OpenMessage() {

        yield return new WaitForSeconds(mTimeDelayToOpen);

        if (mUserIconObject != null) {
            mUserIconObject.SetActive(true);
        }
        else if (this.name != "Status Text Message") {
            Debug.LogWarning("UserIcon Object on " + gameObject.name + " is null.");
        }

        StartCoroutine(OpenTextMessage());
    }

    protected virtual IEnumerator OpenTextMessage() {

        mAnimatingObject.SetActive(true);

        float animationTime = mAnimatingObject.GetComponent<Animator>().runtimeAnimatorController.animationClips[0].length;

        yield return new WaitForSeconds(animationTime);

        mTextGameObject.SetActive(true);

        FinishMessageChain();
    }

    protected virtual void FinishMessageChain() {

    }
}
