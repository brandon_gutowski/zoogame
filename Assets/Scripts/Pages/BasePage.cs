﻿using UnityEngine;
using System.Collections;

public class BasePage : MonoBehaviour {
    
    public BasePage mNextPage;
    private StorySystem mStory;

    public void SetStoryPage(StorySystem story) {
        if(mStory == null) {
            mStory = story;
        }        
    }

    public void NextPage()
    {
        mNextPage.OpenPage(this);
    }

    public void OpenPage(BasePage previousPage)
    {
        this.gameObject.SetActive(true);
        previousPage.ClosePage();

    }

    public void ClosePage()
    {
        Debug.Assert(mStory != null);
        mStory.PageFinished(this);
        this.gameObject.SetActive(false); 
    }

    //Fade in and fade out functions
}
