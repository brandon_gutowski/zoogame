﻿using UnityEngine;
using System.Collections;

public class TextMessage : Message {

    public TextMessagePage mTextMessagePage;

    protected override void FinishMessageChain() {
        if (mNextMessage != null) {
            mNextMessage.OpenMessageFunction();
        }
        else if(mTextMessagePage != null){
            mTextMessagePage.CompletedConversation();
        }
       
    }
}
