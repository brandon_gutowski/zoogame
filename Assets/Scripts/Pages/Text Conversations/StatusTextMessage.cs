﻿using UnityEngine;
using System.Collections;

public class StatusTextMessage : TextMessage {

    protected override IEnumerator OpenTextMessage() {

        yield return new WaitForEndOfFrame();

        mTextGameObject.SetActive(true);

        if (mNextMessage != null) {
            mNextMessage.OpenMessageFunction();
        }
        else if (mTextMessagePage != null) {
            mTextMessagePage.CompletedConversation();
        }
    }
}
