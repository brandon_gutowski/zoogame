﻿using UnityEngine;
using System.Collections;

public class TextMessagePage : BasePage {

    public TextMessage mFirstTextMessage;
    public float mStartingDelay;
    public GameObject mStartingImage;
    public GameObject mTextMessageBackgroundScreen;

    public GameObject mDisconnectButton;

    void Start() {
        StartCoroutine(StartTextMessages());
    }

    private IEnumerator StartTextMessages() {

        if(mStartingImage != null) {

            yield return new WaitForSeconds(mStartingDelay);

            mStartingImage.SetActive(false);
            mTextMessageBackgroundScreen.SetActive(true);
        }
        else {
            yield return new WaitForSeconds(mStartingDelay / 2);
        }        

        yield return new WaitForSeconds(mStartingDelay / 2);

        mFirstTextMessage.OpenMessageFunction();
    }

    public void CompletedConversation() {
        StartCoroutine(CompletedConversationCoroutine());
    }

    public IEnumerator CompletedConversationCoroutine() {

        yield return new WaitForSeconds(2.0f);//Delay the button appearing so the player doesnt feel rushed
        mDisconnectButton.SetActive(true);
    }

    //Store all text Messages hidden under an object somewhere and add them 1 by 1 to the content area.
    //Make the content area start from the button and then everything will just fill upwards and the scroll view will do the reorganizing for me

}
