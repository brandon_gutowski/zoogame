﻿using UnityEngine;
using System.Collections;

public class TextMessagePicture : TextMessage {

    public GameObject mPictureObject;

    protected override IEnumerator OpenTextMessage() {

        yield return new WaitForEndOfFrame();

        mPictureObject.SetActive(true);        

        if (mNextMessage != null) {
            mNextMessage.OpenMessageFunction();
        }
    }
}
