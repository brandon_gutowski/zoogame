﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerInputTextMessage : TextMessage {

    public PlayerInputPanel mPlayerOptions;//Leave this null if the information was updated earlier
    //each text must have an [Input] in it

    private bool mFirstTime = true;

	protected override IEnumerator OpenTextMessage() {

        yield return new WaitForEndOfFrame();

        TriggerPlayerOptions();

    }

    private void TriggerPlayerOptions() {

        if(mPlayerOptions != null  && mFirstTime) {
            mFirstTime = false;
            mPlayerOptions.mPlayerInputMessageToNotify = this;
            mPlayerOptions.gameObject.SetActive(true);
        }
        else {
            CompletedUpdates();
        }        
    }

    public void ModifyTextObjectWithPlayerInput(string input) {
        
        string currentText = mTextGameObject.GetComponent<Text>().text;
        currentText = currentText.Replace("[Input]", input);
        mTextGameObject.GetComponent<Text>().text = currentText;
    }

    public void CompletedUpdates() {
        StartCoroutine(AnimatePlayerInputTextMessage());
    }

   private IEnumerator AnimatePlayerInputTextMessage() {

        mAnimatingObject.SetActive(true);

        float animationTime = mAnimatingObject.GetComponent<Animator>().runtimeAnimatorController.animationClips[0].length;

        yield return new WaitForSeconds(animationTime);

        mTextGameObject.SetActive(true);

        if (mNextMessage != null) {
            mNextMessage.OpenMessageFunction();
        }
    }
}
