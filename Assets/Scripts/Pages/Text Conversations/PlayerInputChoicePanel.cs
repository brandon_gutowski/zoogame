﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerInputChoicePanel : PlayerInputPanel {

    public List<TextMessage> mNextMessageChoices;
    
    public void CompletedUpdates(int playerChoiceInt) {
        //Need to close the first panel incase it is not the choosen panel
        mNextMessageChoices[0].gameObject.SetActive(false);
        mNextMessageChoices[playerChoiceInt].OpenMessageFunction();

        this.gameObject.SetActive(false);
    }
}
