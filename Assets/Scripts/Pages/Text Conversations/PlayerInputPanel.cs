﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerInputPanel : MonoBehaviour {

    [HideInInspector]
    public PlayerInputTextMessage mPlayerInputMessageToNotify;

    public List<PlayerInputTextMessage> mTextMesagesToUpdate;

    public void PlayerSelectedOption(string selection) {
        for (int i = 0; i < mTextMesagesToUpdate.Count; i++) {

            mTextMesagesToUpdate[i].ModifyTextObjectWithPlayerInput(selection);
        }

        mPlayerInputMessageToNotify.CompletedUpdates();
        this.gameObject.SetActive(false);
    }
}
