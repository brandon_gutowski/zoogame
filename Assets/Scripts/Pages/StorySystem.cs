﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StorySystem : MonoBehaviour {

    public PlayerProfile mPlayerProfile;
    public StoryDataSystem mStoryDataSystem;
    public List<BasePage> mAllPages;
    [HideInInspector]public int mCurrentPlayerPage = 0;

    private string mGameMainScene = "GameMainMenu";

    private void Awake() {
        AssignAllPagesStory();
        //mPlayerProfile.SetCurrentStory(this.mStoryDataSystem);       
    }

    private void Start() {
        CloseAllPages();
        mCurrentPlayerPage = mStoryDataSystem.mCurrentPage;
        mAllPages[mCurrentPlayerPage].gameObject.SetActive(true);
    }

    private void Update() {

        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Backspace)) {
            UpdateDataSystem();
            SceneManager.LoadScene(mGameMainScene);
        }
    }

    public void AssignAllPagesStory() {
        for (int i = 0; i < mAllPages.Count; i++) {
            mAllPages[i].SetStoryPage(this);
        }
    }

    public void PageFinished(BasePage page) {

        mCurrentPlayerPage++;

        for(int i = 0; i < mAllPages.Count - 1; i++) {
            if(mAllPages[i].GetInstanceID() == page.GetInstanceID()) {
                
                Debug.Assert(mCurrentPlayerPage == i + 1);
                mCurrentPlayerPage = i + 1;
            }
        }

        if(mCurrentPlayerPage == mAllPages.Count) {
            mPlayerProfile.CurrentStoryCompleted();
            mStoryDataSystem.mIsCompleted = true;
            mPlayerProfile.mCurrentStory = null;
        }
    }

    private void CloseAllPages() {
        for(int i = 0; i < mAllPages.Count; i++) {
            mAllPages[i].gameObject.SetActive(false);
        }
    }

    private void UpdateDataSystem() {
        mStoryDataSystem.mCurrentPage = mCurrentPlayerPage;
    }
}
