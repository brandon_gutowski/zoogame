﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class MapView : MonoBehaviour {

    public GameObject mPlayerIcon;
    public GameObject mMap;

    public List<GPSAreaHolder> mInvalidSpaces;
    public GPSAreaHolder mValidSpace;
    public GPSPolygonHolder mViewBoundary;//View boundary should always start in the lower left of the map and go, upper left, upper right, lower right for the 4 points.

    //private float mScaleFactor = 1.4f;
    //private BoundedValue mScaleHeightBounded;
    //private BoundedValue mScaleWidthBounded; 

    void Awake() {

        if(mViewBoundary.mGPSArea.mGPSPoints.Count != 4) {
            Debug.LogWarning("mVeiwBoundary needs to have 4 points exactly");
        }

        mViewBoundary.mGPSArea.AssignVectors();

        GPSManager.GPSUpdated += UpdatePlayerLocation;

       /* Debug.Assert(this.GetComponent<RectTransform>() != null);
        float widthCurrent = mMap.GetComponent<RectTransform>().sizeDelta.x;
        float heightCurrent = mMap.GetComponent<RectTransform>().sizeDelta.y;

        //Current width and height should start at the maximum size
        mScaleWidthBounded = new BoundedValue((widthCurrent * .25f), widthCurrent);
        mScaleHeightBounded = new BoundedValue((heightCurrent * .25f), heightCurrent);*/
        
    }

    void OnDestroy() {
        GPSManager.GPSUpdated -= UpdatePlayerLocation;
    }

    void Update() {

    }

    /*private void Zoom(float zoomValue) {

        //RectTransform tempRect = ScaleTheMap(zoomValue);
        //Check if anything is not being seen
        //Adjust correctly


    }

    private RectTransform ScaleTheMap(float zoomValue) {

        RectTransform rect = mMap.GetComponent<RectTransform>();

        if(zoomValue > 0) {
            mScaleHeightBounded.IncreaseValue(mScaleFactor);
            mScaleWidthBounded.IncreaseValue(mScaleFactor);   
        }
        else {
            mScaleHeightBounded.DecreaseValue(mScaleFactor);
            mScaleWidthBounded.DecreaseValue(mScaleFactor);
        }

        rect.localScale = UpdateScale(rect.localScale);

        return rect;
    }

    private Vector2 UpdateScale(Vector2 incoming) {

        incoming.x = mScaleWidthBounded.currentValue;
        incoming.y = mScaleHeightBounded.currentValue;

        return incoming;

    }*/

    public void UpdatePlayerLocation(GPSPoint playerLocation) {

        //GPSPoint playerLocation = GPSManager.mPlayerGPSPoint;
        Vector2 newPlayerLocation = Vector2.one;
        
        if (mValidSpace.GetType() == typeof(GPSPathHolder)) {
            GPSPath  path = mValidSpace.gameObject.GetComponent<GPSPathHolder>().mPath;
            newPlayerLocation = path.PositionOnPath(playerLocation);
        }
        else if(mValidSpace.GetType() == typeof(GPSPolygonHolder)){
            GPSPolygon polygon = mValidSpace.gameObject.GetComponent<GPSPolygonHolder>().mGPSArea;
            newPlayerLocation = polygon.PointWithinArea(playerLocation);
        }

        UpdateIconLocation(newPlayerLocation);        
    }

    private void UpdateIconLocation(Vector2 playerLocation) {

        RectTransform rectTransform = mMap.GetComponent<RectTransform>();

        Vector2 zeroZeroCornerMap = Vector2.zero;
        zeroZeroCornerMap.x = - rectTransform.rect.width/2f;
        zeroZeroCornerMap.y = -(rectTransform.rect.height/2f);

        Vector2 upperRightCornerIRL = mViewBoundary.mGPSArea.mGPSVectorMtrs[2];

        //linear interpolation

        Vector2 tempIconLocation = Vector2.zero;
        tempIconLocation.y = playerLocation.y * (rectTransform.rect.height) / upperRightCornerIRL.y;
        tempIconLocation.x = playerLocation.x * (rectTransform.rect.width) / upperRightCornerIRL.x;

        tempIconLocation.y += zeroZeroCornerMap.y;
        tempIconLocation.x += zeroZeroCornerMap.x;

        mPlayerIcon.GetComponent<RectTransform>().localPosition = tempIconLocation;
    }

    private Vector2 RotateVector(Vector2 vector, int numberOf90degrees) {

        Vector2 newVector = vector;

        switch (numberOf90degrees) {
            default:
                break;

            case 1:
                newVector.x = vector.y;
                newVector.y = vector.x;
                break;
        }

        return newVector;
    }


}
