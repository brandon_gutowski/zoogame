﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationPage : BasePage {

    public List<AnimationScene> mAnimationScenes;
    public float mStartDelayTime = 2f;
    private int mSceneActivationCounter = 0;

   void Start() {
        StartCoroutine(StartAnimation());
       
    }

    public void CompletedScene() {
        mSceneActivationCounter++;
        if(mSceneActivationCounter < mAnimationScenes.Count) {
            mAnimationScenes[mSceneActivationCounter - 1].gameObject.SetActive(false);
            mAnimationScenes[mSceneActivationCounter].gameObject.SetActive(true);
            mAnimationScenes[mSceneActivationCounter].StartAnimationText();
        }else {
            CompleteAnimation();
        }
    }

    private void CompleteAnimation() {
        NextPage();
    }

    IEnumerator StartAnimation() {

        yield return new WaitForSeconds(mStartDelayTime);

        mAnimationScenes[mSceneActivationCounter].StartAnimationText();
    }
}
