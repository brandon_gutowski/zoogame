﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GPSStoryHub : MonoBehaviour {

    public GPSPuzzlePage mLookingForNewLocationPage; //First Page is always the opening Page
    public DebugGPSPage mWrongLocationPage;
    public Text mLocationUpdateText;

    public Text mText;
    public List<BasePage> mHubPages;

    public void GPSCheck() {

        bool hasFoundAPage = false;
        float closestDistance = 1000f;

        for (int i = 0; i < mHubPages.Count; i++) {

            GPSPointHolder currentPageGPSSolution = mHubPages[i].GetComponent<GPSPointHolder>();

            if (currentPageGPSSolution.mGPSPoint.AreGPSPointsNear(closestDistance)) {

                closestDistance = currentPageGPSSolution.mGPSPoint.DistancefromPlayer;

                if (!currentPageGPSSolution.mHasBeenVisited) {
                    mLocationUpdateText.text = currentPageGPSSolution.mGPSPoint.DistancefromPlayer.ToString();
                    mHubPages[i].GetComponent<BasePage>().OpenPage(mLookingForNewLocationPage);

                    hasFoundAPage = true;

                    break;
                }
                else {
                    closestDistance = currentPageGPSSolution.mGPSPoint.DistancefromPlayer;
                }
            }
        }

        mLocationUpdateText.text = GPSManager.mPlayerGPSLocation.latitude.ToString() + ", " + GPSManager.mPlayerGPSLocation.longitude.ToString();

        if (!hasFoundAPage) {
            mWrongLocationPage.UpdateText();
            mWrongLocationPage.OpenPage(mLookingForNewLocationPage);
        }
    }

    public void UpdateText(string text) {
        mText.text = text;
    }
}
