﻿using UnityEngine;
using System.Collections;

public class GPSPolygonHolder : GPSAreaHolder {

    public new GPSPolygon mGPSArea;

   void Awake() {
        mGPSArea.AssignVectors(mGPSArea.mRefrencePoint);
    }

}
