﻿using UnityEngine;
using System.Collections;

public class GPSFunctions {

    public static float mConversionToMeters = 6371000;

    public static Vector2 GPSPointToVector(GPSPoint point) {
        Vector2 vector = Vector2.one;

        vector.x = (float)point.mLongitude;
        vector.y = (float)point.mLatitude;

        return vector;
    }

    public static Vector2 GPSPointToMtrVector(GPSPoint point, GPSPoint refrencePoint) {
        Vector2 vector = Vector2.one;

        float lat1 = (float) point.mLatitude;
        float lon1 = (float) point.mLongitude;
        float lat2 = (float) refrencePoint.mLatitude;
        float lon2 = (float) refrencePoint.mLongitude;

        float latDouble = DistanceBetween(lat1, lon2, lat2, lon2);
        float lonDouble = DistanceBetween(lat2, lon1, lat2, lon2);

        if (lat1 > lat2) latDouble *= -1;
        if (lon1 > lon2) lonDouble *= -1;

        vector.x = (float)lonDouble;
        vector.y = (float)latDouble;

        return vector;
    }

    //MAKE THIS covert back from refrence dependant
    /*public static GPSPoint VectorToGPSPoint(Vector2 vector) {

        GPSPoint point = new GPSPoint();

        point.mLatitude = vector.y;
        point.mLongitude = vector.x;

        return point;

    }*/

    //Using the Haversine formula and its in meters
    public static float DistanceBetweenGPSPoints(GPSPoint point1, GPSPoint point2) {

        float lat1 = (float) point1.mLatitude;
        float lon1 = (float) point1.mLongitude;
        float lat2 = (float) point2.mLatitude;
        float lon2 = (float) point2.mLongitude;

        return DistanceBetween(lat1, lon1, lat2, lon2);        
    }

    //This is wrong
    /*public static float DistanceBetweenRelativeGPSPoints(Vector2 vector1, Vector2 vector2) {
        float lat1 = vector1.x;
        float lon1 = vector1.y;
        float lat2 = vector2.x;
        float lon2 = vector2.y;

        return DistanceBetween(lat1, lon1, lat2, lon2);
    }*/

    public static float DistanceBetween(float lat1, float lon1, float lat2, float lon2) {
        //convertToRads
        float thetaInRads = deg2rad(lon2 - lon1);
        float phiInRads = deg2rad(lat2 - lat1);
        lat1 = deg2rad(lat1);
        lat2 = deg2rad(lat2);

        float a = Mathf.Sin(phiInRads / 2f) * Mathf.Sin(phiInRads / 2f) + Mathf.Cos(lat1) * Mathf.Cos(lat2) * Mathf.Sin(thetaInRads / 2f) * Mathf.Sin(thetaInRads / 2f);
        float c = 2f * Mathf.Atan2(Mathf.Sqrt(a), Mathf.Sqrt(1 - a));

        float dist = c * mConversionToMeters;

        return (dist);
    }

    public static float deg2rad(float deg) {
        return (deg * Mathf.PI / 180.0f);
    }

    public static float rad2deg(float rad) {
        return (rad / Mathf.PI * 180.0f);
    }

}
