﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class GPSPolygon:GPSArea {

    private float mXMax;
    private float mXMin;
    private float mYMax;
    private float mYMin;
    private GPSPoint mCenterPoint;

    private struct Side {
        public Vector2 point1;
        public Vector2 point2;
    }

    public GPSPolygon(List<GPSPoint> points) {
        Debug.Assert(points.Count > 2);
        this.mGPSPoints = points;
        this.AssignVectors();

        DetermineCenter();

    }

    public GPSPolygon(List<GPSPoint> points, GPSPoint refrencePoint = null) {

        Debug.Assert(points.Count > 2);
        this.mGPSPoints = points;
        this.AssignVectors(refrencePoint);

        DetermineCenter();

    }

    public GPSPolygon(List<Vector2> vectors, GPSPoint refrencePoint = null) {

        mGPSVectorMtrs = vectors;
        mRefrencePoint = refrencePoint;

        this.mGPSPoints = new List<GPSPoint>();
        GPSPoint temp = new GPSPoint();        

        Debug.Assert(vectors.Count > 2);

        for (int i = 0; i < vectors.Count; i++) {
            temp.mLatitude = vectors[i].y;
            temp.mLongitude = vectors[i].x;

            this.mGPSPoints.Add(temp);
        }

        DetermineCenter();     
    }
        
    public bool IsPlayerWithinArea() {

        GPSPoint playerLocation = GPSManager.mPlayerGPSPoint;

        return IsWithinArea(playerLocation);
    }

    public Vector2 PointWithinArea(GPSPoint point) {

        //Debug.Log("Point: " + point.mLatitude + " , " + point.mLongitude);
        //Debug.Log("Refrence: " + mRefrencePoint.mLatitude + " , " + mRefrencePoint.mLongitude);

        Vector2 pointAsVector2 = GPSFunctions.GPSPointToMtrVector(point, mRefrencePoint);

        //Debug.Log("Vector: " + pointAsVector2);

        if (IsWithinArea(point)) {
            return pointAsVector2;
        }else {
            //Move point to within the polygon
            return pointAsVector2;
        }
    }

    public bool IsWithinArea(GPSPoint GPSPoint) {
        //Convert To Relative Position
        Vector2 GPSVectorRelative = GPSFunctions.GPSPointToMtrVector(GPSPoint, mRefrencePoint);

        if (IsWithinBoundingBox(GPSVectorRelative)) {
            //Debug.Log("Within Area " + GPSVectorRelative.x + " , " + GPSVectorRelative.y);
            if (NumberOfRaycasts(GPSVectorRelative) % 2 > 0) {
                return true;
            }
            
            return false;
        }

        return false;
    }

    public float DistanceFromPlayer() {
        float distanceFromCenter = GPSFunctions.DistanceBetweenGPSPoints(mCenterPoint, GPSManager.mPlayerGPSPoint);
        float polygonLength = ((mXMax - mXMin) > (mYMax - mYMin)) ? (mXMax - mXMin) : (mYMax - mYMin);

        float distanceFromEdge = distanceFromCenter - polygonLength;

        return distanceFromEdge;
    }

    private bool IsWithinBoundingBox(Vector2 gpsVectorRelative) {

        mXMax = mGPSVectorMtrs[0].x;
        mXMin = mGPSVectorMtrs[0].x;
        mYMax = mGPSVectorMtrs[0].y;
        mYMin = mGPSVectorMtrs[0].y;

        for(int i = 1; i < mGPSVectorMtrs.Count; i++) {

            if(mGPSVectorMtrs[i].x > mXMax) {
                mXMax = mGPSVectorMtrs[i].x;
            }
            else if(mGPSVectorMtrs[i].x < mXMin) {
                mXMin = mGPSVectorMtrs[i].x;
            }

            if (mGPSVectorMtrs[i].y > mYMax) {
                mYMax = mGPSVectorMtrs[i].y;
            }
            else if (mGPSVectorMtrs[i].y < mYMin) {
                mYMin = mGPSVectorMtrs[i].y;
            }
        }

        //Debug.Log(mXMin + " , " + mXMax + " , " + mYMin + " , " + mYMax);

        if (gpsVectorRelative.x < mXMin || gpsVectorRelative.x > mXMax || gpsVectorRelative.y < mYMin || gpsVectorRelative.y > mYMax) {
            return false;
        }

        return true;

    }

    private int NumberOfRaycasts(Vector2 targetPoint) {

        //Debug.Assert(mXMax > 0 || mXMin > 0 || mYMax > 0 || mYMin > 0);
        
        List<Side> sides = AssignSides();

        float bufferSpaceAroundArea = ((mXMax - mXMin) / 100f);

        Vector2 rayPoint1;

        if (mXMin < mXMax) {
            rayPoint1 = new Vector2(mXMin - bufferSpaceAroundArea, targetPoint.y);
        }
        else {
            rayPoint1 = new Vector2(mXMin + bufferSpaceAroundArea, targetPoint.y);
        }
        Side ray;
        ray.point1 = rayPoint1;
        ray.point2 = targetPoint;

        int intersections = 0;

        for(int i = 0; i < sides.Count; i++) {
            if(SidesIntersect(ray, sides[i])) {
                intersections++;
            }
        }
        //Debug.Log("RayPoint " + rayPoint1.x + " ," + rayPoint1.y + " : " + targetPoint.x);
        //Debug.Log("Number of intersections " + intersections);
        return intersections;
    }

    private List<Side> AssignSides() {
        List<Side> sides = new List<Side>();

        Side tempSide;

        for(int i = 0; i < mGPSVectorMtrs.Count -1; i++) {
            tempSide.point1 = mGPSVectorMtrs[i];
            tempSide.point2 = mGPSVectorMtrs[i+1];
            sides.Add(tempSide);
        }

        //Add the side connecting first point to last point
        tempSide.point1 = mGPSVectorMtrs[0];
        tempSide.point2 = mGPSVectorMtrs[mGPSVectorMtrs.Count - 1];
        sides.Add(tempSide);

        return sides;
    }

    private bool SidesIntersect(Side ray, Side polygonSide) {
        //using the forms of Ax+ By = C to do the math for Line-Line Intersection
        //https://www.topcoder.com/community/data-science/data-science-tutorials/geometry-concepts-line-intersection-and-its-applications/

        double A_ray = ray.point2.y - ray.point1.y;
        double B_ray = ray.point1.x - ray.point2.x;
        double C_ray = A_ray * ray.point1.x + B_ray * ray.point1.y;

        double A_side = polygonSide.point2.y - polygonSide.point1.y;
        double B_side = polygonSide.point1.x - polygonSide.point2.x;
        double C_side = A_side * polygonSide.point1.x + B_side * polygonSide.point1.y;

        double det = A_ray * B_side - A_side * B_ray;
        double intersectionPointX;
        double intersectionPointY;

        if (det == 0) {
            //Lines are parallel
            return false;
        }
        else {
            intersectionPointX = (B_side * C_ray - B_ray * C_side) / det;
            intersectionPointY = (A_ray * C_side - A_side * C_ray) / det;

            //Debug.Log("InteresectionPoint: " + intersectionPointX + " , " + intersectionPointY);
        }

        //Min and max points of each line to determine if the intersection points above are on the line
        float rayXMinValue = ray.point1.x > ray.point2.x ? ray.point2.x : ray.point1.x;
        float rayXMaxValue = ray.point1.x < ray.point2.x ? ray.point2.x : ray.point1.x;
        float rayYMinValue = ray.point1.y > ray.point2.y ? ray.point2.y : ray.point1.y;
        float rayYMaxValue = ray.point1.y < ray.point2.y ? ray.point2.y : ray.point1.y;

        int decimalPlaceFactor = 10000;
        //Rounding Values in case of straight lines
        rayXMinValue = Mathf.Floor(rayXMinValue * decimalPlaceFactor) / decimalPlaceFactor;
        rayXMaxValue = Mathf.Ceil(rayXMaxValue * decimalPlaceFactor) / decimalPlaceFactor;
        rayYMinValue = Mathf.Floor(rayYMinValue * decimalPlaceFactor) / decimalPlaceFactor;
        rayYMaxValue = Mathf.Ceil(rayYMaxValue * decimalPlaceFactor) / decimalPlaceFactor;

        //Ray min and Max are exactly the same
        float sideXMinValue = polygonSide.point1.x > polygonSide.point2.x ? polygonSide.point2.x : polygonSide.point1.x;
        float sideXMaxValue = polygonSide.point1.x < polygonSide.point2.x ? polygonSide.point2.x : polygonSide.point1.x;
        float sideYMinValue = polygonSide.point1.y > polygonSide.point2.y ? polygonSide.point2.y : polygonSide.point1.y;
        float sideYMaxValue = polygonSide.point1.y < polygonSide.point2.y ? polygonSide.point2.y : polygonSide.point1.y;

        //Debug.Log("Side: " + sideXMinValue + " ," + sideXMaxValue + " , " + sideYMinValue + " , " + sideYMaxValue);

        //Check if its on the X value of the first line
        if ((rayXMinValue <= intersectionPointX) && (intersectionPointX <= rayXMaxValue)) {
            //Debug.Log("Within Ray X");
            if((sideXMinValue <= intersectionPointX) && (intersectionPointX <= sideXMaxValue)) {
                //Debug.Log("Within Side X: " + rayXMinValue + "<=" + intersectionPointY + " <=" + rayYMaxValue);
                if ((rayYMinValue <= intersectionPointY) && intersectionPointY <= rayYMaxValue) {
                    //Debug.Log("Within Ray Y");
                    if ((sideYMinValue <= intersectionPointY) && (intersectionPointY <= sideYMaxValue)) {
                        //Debug.Log("Within Side Y");
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /*private Vector2 MoveInsideArea(Vector2 relativeLocation) {

        if()
        return relativeLocation;
    }

    private Vector2 MoveOutsideArea(Vector2 relativeLocation) {

        return relativeLocation;
    }*/

    private void DetermineCenter() {
        //Uses the centriod of a polygon formula.  Polygon must be a non-self-intersecting closed polygon 
        //vertices are assumed to be numbered in order of their occurrence along the polygon's perimeter.
        //https://en.wikipedia.org/wiki/Centroid -> Centroid of a polygon

        double area = GetArea();
        double centroidx = 0;
        double centroidy = 0;

        for(int i = 0; i < mGPSPoints.Count - 1; i++) {
            double frontHalfx = (mGPSPoints[i].mLongitude + mGPSPoints[i + 1].mLongitude);
            double frontHalfy = (mGPSPoints[i].mLatitude + mGPSPoints[i + 1].mLatitude);
            double backHalf = (mGPSPoints[i].mLongitude * mGPSPoints[i + 1].mLatitude - mGPSPoints[i + 1].mLongitude * mGPSPoints[i].mLatitude);

            centroidx += (frontHalfx * backHalf);
            centroidy += (frontHalfy * backHalf);
        }

        centroidx = centroidx / (6d * area);
        centroidy = centroidy / (6d * area);

        mCenterPoint = new GPSPoint(centroidy, centroidx);
    }

    private double GetArea() {
        double area = 0;
        double signedAreaPart;

        for(int i = 0; i < mGPSPoints.Count - 1; i++) {
            signedAreaPart = mGPSPoints[i].mLongitude * mGPSPoints[i + 1].mLatitude - mGPSPoints[i + 1].mLongitude * mGPSPoints[i].mLatitude;
            area += signedAreaPart;
        }

        area /= 2;

        return area;
    }
}
