﻿using UnityEngine;
using System.Collections.Generic;

public class GPSArea {

    public List<GPSPoint> mGPSPoints;
    public GPSPoint mRefrencePoint;
    [HideInInspector]
    public List<Vector2> mGPSVectorMtrs = new List<Vector2>();//Vectors are always converted to meters

    void Awake() {
        if(mGPSVectorMtrs.Count == 0) {
            AssignVectors();
        }
    }

    public void AssignVectors(GPSPoint refrencePoint = null) {
        Vector2 temp = new Vector2();

        if(refrencePoint != null) {
            mRefrencePoint = refrencePoint;
        }

        Debug.Assert(mRefrencePoint != null);
        Debug.Assert(mGPSPoints.Count > 0);

        for (int i = 0; i < mGPSPoints.Count; i++) {
            temp = GPSFunctions.GPSPointToMtrVector(mGPSPoints[i], mRefrencePoint);
            mGPSVectorMtrs.Add(temp);
        }
    }

    
}
