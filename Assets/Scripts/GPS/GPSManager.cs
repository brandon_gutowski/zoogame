﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GPSManager : MonoBehaviour {

    public static LocationInfo mPlayerGPSLocation = new LocationInfo(); //use struct Location Info
    public static GPSPoint mPlayerGPSPoint = new GPSPoint();
    public static GPSPoint mZooRefrencePoint = new GPSPoint(42.474803f, -83.162876f);//always the bottom left corner of the zoo
    public static int mNearbyDistance = 50; //meters
    public static bool mGPSIsPaused = false;
    //public static string mDebugString = "";

    public delegate void GPSPointUpdate(GPSPoint mPoint);
    public static event GPSPointUpdate GPSUpdated;
    public static void OnGPSUpdated(GPSPoint point) {
        if (GPSUpdated != null) {
            GPSUpdated.Invoke(point);
        }
    }

    public int mTimeOutTime = 20;
    public int mQualityTimeSeconds  = 30;
    public GameObject mLoadingObject;
    public Text mLoadingDebugString;

    private bool mCheckingLocation = true;
    private float mGPSUpdateTime = 2.0f;

	void Awake () {
        
        if (!mGPSIsPaused) {
            StartCoroutine(InitializeGPS());
        }        
    }

	void Update () {
        if (!mGPSIsPaused) {
            if (Input.location.status == LocationServiceStatus.Running && !mCheckingLocation) {
                StartCoroutine(CheckGPSLocation());               
            }
        }       
	}

    //This is placed above Focus in hopes it is called and handled before Focus
    void OnApplicationPause(bool state) {
        if (state) {
            Input.location.Stop();
        }

        mGPSIsPaused = state;
        
    }

    void OnApplicationFocus(bool state) {

        if (state) {
            if (!mCheckingLocation) {
                StartCoroutine(InitializeGPS());
            }
                       
        }
        else {
           Input.location.Stop();
        }
    }

    /*Function Check the GPS location every so often*/
    IEnumerator CheckGPSLocation() {
        mCheckingLocation = true;
       
        if(mLoadingObject != null) {
            mLoadingDebugString.text = "Checking";
            mLoadingObject.SetActive(false);
        }
        
        if (IsBetterLocation(mPlayerGPSLocation, Input.location.lastData)) {
            mPlayerGPSLocation = Input.location.lastData;
            UpdateGPSPoint();
        }

        yield return new WaitForSeconds(mGPSUpdateTime);
        if (mLoadingObject != null) {
            mLoadingDebugString.text = "Done Check";
            mLoadingObject.SetActive(false);
        }

        mCheckingLocation = false;        
    }

    public void UpdateGPSLocationManually() {
        StopCoroutine(CheckGPSLocation());
        StartCoroutine(CheckGPSLocation());
    }

    private bool IsBetterLocation(LocationInfo currentBestLocation, LocationInfo location) {

        if(currentBestLocation.latitude == 0) {
            return true;
        }

        float waitUpdateTimeMS = mQualityTimeSeconds;//Timestamp is in seconds, so convert minutes to seconds

        double timeDelta = currentBestLocation.timestamp - location.timestamp;
        bool isSignificantlyNewer = timeDelta > waitUpdateTimeMS;
        bool isSignificantlyOlder = timeDelta < -waitUpdateTimeMS;
        bool isNewer = timeDelta > 0;
        bool isAlotNewer = (timeDelta + waitUpdateTimeMS/4f) > 0;

        if (isSignificantlyNewer) {
            return true;
        }
        else if(isSignificantlyOlder){
            return false;
        }

        float deltaAccuracy = location.horizontalAccuracy - currentBestLocation.horizontalAccuracy;

        bool isLessAccurate = deltaAccuracy > 0;
        bool isMoreAccurate = deltaAccuracy < 0;
        bool isSignificantlyLessAccurate = deltaAccuracy > 100;

        if (isMoreAccurate) {
            return true;
        }else if(isNewer && isLessAccurate) {
            return true;
        }else if(isAlotNewer && !isSignificantlyLessAccurate) {
            return true;
        }

        return false;
    }

    IEnumerator InitializeGPS() {
        
        if (!Input.location.isEnabledByUser) {
            Debug.Log("GPS not enabled by User");
            if (mLoadingDebugString != null)
                mLoadingDebugString.text = "Not Enabled";
            yield break;
        }

        Input.location.Start();
        if (mLoadingObject != null) {
            mLoadingObject.SetActive(true);
            mCheckingLocation = true;
        }

        while (Input.location.status == LocationServiceStatus.Initializing && mTimeOutTime > 0) {
            yield return new WaitForSeconds(1);
            mTimeOutTime--;
        }

        if (mTimeOutTime < 1) {
            Debug.LogWarning("Timed Out");
            if (mLoadingDebugString != null) 
                mLoadingDebugString.text = "Timed Out";
            yield break;
        }

        if (LocationServiceStatus.Failed == Input.location.status) {
            Debug.Log("Unable To Determine Location");
            if (mLoadingDebugString != null)
                mLoadingDebugString.text = "Unable To Determine Location";

            yield break;
        }
        else {
            if (mLoadingObject != null) {
                mLoadingDebugString.text = "success";
                mLoadingObject.SetActive(false);
            }
            mGPSIsPaused = false;
            mCheckingLocation = false;
            mPlayerGPSLocation = Input.location.lastData;
            UpdateGPSPoint();            
        }
        if (mLoadingObject != null) {
            mLoadingObject.SetActive(false);
        }
        mGPSIsPaused = false;
        mCheckingLocation = false;
    } 

    private void UpdateGPSPoint() {
        mPlayerGPSPoint.mLatitude = mPlayerGPSLocation.latitude;
        mPlayerGPSPoint.mLongitude = mPlayerGPSLocation.longitude;

        OnGPSUpdated(mPlayerGPSPoint);
    }

}
