﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GPSStatusDisplay : MonoBehaviour {

    public void OnSceneLoaded() {
        GPSManager manager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GPSManager>();

        if(manager != null) {
            manager.mLoadingObject = gameObject;
        }
    }
}
