﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class GPSPath: GPSArea {

    public float mWidth;//This needs to be in meters, it will be converted out of meters for use in this function

    public float mSnapWidth;

    public GPSPath(List<GPSPoint> points, float width, float snapWidth, GPSPoint refrencePoint) {
        this.mGPSPoints = points;
        this.mWidth = width;
        this.mSnapWidth = snapWidth;
        this.mRefrencePoint = refrencePoint;
        AssignVectors();
    }

    public Vector2 PositionOnPath(GPSPoint mIncomingPosition) {

        //float testDistance = GPSFunctions.DistanceBetweenGPSPoints(mIncomingPosition, mRefrencePoint);
        

        Vector2 relativePathPosition = GPSFunctions.GPSPointToMtrVector(mIncomingPosition, mRefrencePoint);
        //Debug.Log("RelativePathPosition " + relativePathPosition);

        int gpsVectorListInt = FindNearestPointInt(relativePathPosition);
        Vector2 nearestPoint = mGPSVectorMtrs[gpsVectorListInt];
        Vector2 secondNearestPoint = FindNearestToTwoPoints(relativePathPosition, gpsVectorListInt);

        //Is within path + width
        GPSPolygon widthArea = CreatePathWidth(nearestPoint, secondNearestPoint, mWidth);

        if (widthArea.IsWithinArea(mIncomingPosition)){
            //Debug.Log("Width in");
            return relativePathPosition;
        }

        //Is within path + snapwidth
        widthArea = CreatePathWidth(nearestPoint, secondNearestPoint, mSnapWidth);
        if (widthArea.IsWithinArea(mIncomingPosition)) {
            //Snap to the edge
            relativePathPosition = SnapToPath(nearestPoint, secondNearestPoint, relativePathPosition);
            //Debug.Log("Snapping");
            return relativePathPosition;
        }
        else {
            //Debug.Log("Outside");
            return relativePathPosition;
        }
    }

    private int FindNearestPointInt(Vector2 point) {

        int nearestInt = 0;

        if(mGPSVectorMtrs.Count == 0) {
            AssignVectors();
        }

        float nearestDistance = Vector2.Distance(point, mGPSVectorMtrs[0]);

        for (int i = 0; i < mGPSVectorMtrs.Count; i++) {

            float distance = Vector2.Distance(point, mGPSVectorMtrs[i]);
            if(distance < nearestDistance) {
                nearestDistance = distance;
                nearestInt =i;
            }
        }

        return nearestInt;
    }

    private Vector2 FindNearestToTwoPoints(Vector2 point, int mGPSInt) {

        float distance1;
        float distance2;

        if(mGPSInt == 0) return mGPSVectorMtrs[1];
        if(mGPSPoints.Count == 2) return mGPSVectorMtrs[0];
        
        distance1 = Vector2.Distance(point, mGPSVectorMtrs[mGPSInt - 1]);
        distance2 = Vector2.Distance(point, mGPSVectorMtrs[mGPSInt + 1]);

        if (distance1 < distance2) {
            return mGPSVectorMtrs[mGPSInt - 1];
        }
        else {
            return mGPSVectorMtrs[mGPSInt + 1];
        }
        
    }

    private GPSPolygon CreatePathWidth(Vector2 p1, Vector2 p2, float width) {

        //Debug.Log("Point1: " + p1.x + ", " + p1.y);
        //Debug.Log("Point2: " + p2.x + ", " + p2.y);

        List<Vector2> corners = new List<Vector2>(){ Vector2.one, Vector2.one, Vector2.one, Vector2.one };

        Vector2 perpendicularVector = GetPerpendicularVector(p1, p2);
        //Vector2 perpendicularPoint2 = GetPerpendicularVector(p2, p1);
        
        Vector2 corner0 = p1;
        Vector2 corner1 = p1;
        Vector2 corner2 = p2;
        Vector2 corner3 = p2;

        //Vector2 perpendicularVector1 = new Vector2(perpendicularPoint1.x - p1.x, perpendicularPoint1.y - p1.y);
        //Vector2 perpendicularVector2 = new Vector2(perpendicularPoint2.x - p1.x, perpendicularPoint2.y - p1.y);

        //Debug.Log("1: " + perpendicularVector);

        perpendicularVector.Normalize();

        corner0.x += perpendicularVector.x * width;
        corner1.x += perpendicularVector.x * -width;
        corner2.x += perpendicularVector.x * width;
        corner3.x += perpendicularVector.x * -width;

        corners[0] = corner0;
        corners[1] = corner1;
        corners[2] = corner2;
        corners[3] = corner3;

        //Debug.Log(corners[0]);
        //Debug.Log(corners[1]);
        //Debug.Log(corners[2]);
        //Debug.Log(corners[3]);

        GPSPolygon pathArea = new GPSPolygon(corners, mRefrencePoint);

        return pathArea;
    }

    private Vector2 GetPerpendicularVector(Vector2 p1, Vector2 p2) {
        //For information on the solving of this function refer to ZooGame/Technical Documents/GPS Equations Section GPS Path Find perpendicular Line

        Vector2 p3Vector = new Vector2(p2.y - p1.y, -(p2.x - p1.x)) ;      

        return p3Vector;
    }

    //all three incoming points are in relative space
    private Vector2 SnapToPath(Vector2 lineP1, Vector2 lineP2, Vector2 pointOffLineP3) {
        //Need to find the closest point on the line, make a vector from the line point to the player point
        //Then I need to reduce magnitude to 1, then multiple by the width to put it at the edge.

        Vector2 closestPointAlongPathP4 = FindClosestPointOnPath(lineP1, lineP2, pointOffLineP3);

        Debug.Log(closestPointAlongPathP4);

        Vector2 vectorP4ToP3 = new Vector2(pointOffLineP3.x - closestPointAlongPathP4.x, pointOffLineP3.y - closestPointAlongPathP4.y);
        vectorP4ToP3.Normalize();

        Vector2 pointOnEdgeOfPath = closestPointAlongPathP4 + (vectorP4ToP3 * mWidth);

        return pointOnEdgeOfPath;
    }

    private Vector2 FindClosestPointOnPath(Vector2 p1, Vector2 p2, Vector2 p3) {

        Vector2 pointAlongLine = new Vector2();

        double distanceP1P2 = Vector2.Distance(p1, p2);
        double distanceP1P3 = Vector2.Distance(p1, p3);
        double distanceP2P3 = Vector2.Distance(p2, p3);

        double distanceP1P4 = ((distanceP1P2 * distanceP1P2) + (distanceP1P3 * distanceP1P3) - (distanceP2P3 * distanceP2P3)) / (2 * distanceP1P2 * distanceP1P2);

        Vector2 vectorFormP1P2 = new Vector2(p2.x - p1.x, p2.y - p1.y);
      
        pointAlongLine = p1 + vectorFormP1P2.normalized * ((float)distanceP1P4);//dont forget to add the value of the starting point to the new line (aka p1)


        return pointAlongLine;
    }
}
