﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomPropertyDrawer (typeof(GPSPoint))]
public class GPSPointPropertyAttribute : PropertyDrawer {

    //draw prperty within this rectangle
    public override void OnGUI (Rect position, SerializedProperty property, GUIContent label) {

        EditorGUI.BeginProperty(position, label, property);

        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        int indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        Rect latitudeRect = new Rect(position.x, position.y, 80, position.height);
        Rect longitudeRect = new Rect(position.x + 120, position.y, 80, position.height);          

        EditorGUI.PropertyField(latitudeRect, property.FindPropertyRelative("mLatitude"),GUIContent.none);
        EditorGUI.PropertyField(longitudeRect, property.FindPropertyRelative("mLongitude"), GUIContent.none);

        EditorGUI.indentLevel = indent;
        EditorGUI.EndProperty();
    }


    //THis was from a unity post but isnt doing what I wanted.  it shrinks the size of the label but I cannot figure how to put 2 in the same row.
    //GUILayout.BeginHorizontal();
    //GUILayout.Label(new GUIContent("mLat"), GUILayout.Width(50));
    // EditorGUILayout.PropertyField(property, GUIContent.none, GUILayout.MinWidth(100));

    //GUILayout.Label(new GUIContent("mLon"), GUILayout.Width(50));
    //EditorGUILayout.PropertyField(property, GUIContent.none, GUILayout.MinWidth(100));
    //GUILayout.EndHorizontal();
    //GUIContent labelLat = new GUIContent("mLat");
    //GUIContent labelLon = new GUIContent("mLong");     
}
#endif
