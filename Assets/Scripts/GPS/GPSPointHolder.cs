﻿using UnityEngine;
using System.Collections;

public class GPSPointHolder : MonoBehaviour {

    public GPSPoint mGPSPoint;

    [HideInInspector]
    public bool mHasBeenVisited = false;
}
