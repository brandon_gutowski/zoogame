﻿using UnityEngine;

[System.Serializable]
public class GPSPoint {

    public double mLatitude;
    public double mLongitude;

    public GPSPoint(double latitude, double longitude) {        
        this.mLongitude = longitude;
        this.mLatitude = latitude;
    }

    public GPSPoint() {
        this.mLongitude = 0;
        this.mLatitude = 0;
        
    }

    private float mDistanceFromPlayer;
    public float DistancefromPlayer {
        get {
            return mDistanceFromPlayer;
        }
    }

    //I think I need ot make a better screen for debugging on the go to check this
    public bool AreGPSPointsNear(float lastShortestDistance = 1000f) {

        if (GPSManager.mPlayerGPSPoint != null) {
            mDistanceFromPlayer = GPSFunctions.DistanceBetweenGPSPoints(GPSManager.mPlayerGPSPoint, this);

            if (mDistanceFromPlayer <= GPSManager.mNearbyDistance) {
                if (mDistanceFromPlayer < lastShortestDistance) {
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }
}

