﻿using UnityEngine;
using System.Collections;

public class GPSAreaHolder : MonoBehaviour {

    public GPSArea mGPSArea;

    void Awake() {

        if (mGPSArea != null) {
            mGPSArea.AssignVectors();
        }
    }
}
