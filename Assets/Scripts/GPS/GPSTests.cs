﻿#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GPSTests : MonoBehaviour {

    //public GPSArea mArea;
    //public List<GPSPoint> mTestPoints;
    public MapView mMapView;

    void Start() {
        //GPSAreaTest();
        //GPSPathTest();

        StartCoroutine(PlayerIconTests());
    }

	private void GPSAreaTest() {

        List<GPSPoint> corners = new List<GPSPoint>() {
            new GPSPoint(42.498204, -83.110328),// Gardenia Barrington
            new GPSPoint(42.496658, -83.110274),// Forest Barrington
            new GPSPoint(42.496632, -83.111446),//Forest Dartmouth
            new GPSPoint(42.498238, -83.111504)//Gardenia Dartmouth
           };

        GPSPoint insideTest = new GPSPoint(42.497591, -83.110900);
        GPSPoint outsideCloseTest = new GPSPoint(42.498293, -83.109312);
        GPSPoint outsideTest = new GPSPoint(42.501115, 83.104789);

        GPSPolygon area = new GPSPolygon(corners, corners[0]);

        Debug.Log("insideTest is " + area.IsWithinArea(insideTest));       
        Debug.Log("outsideCloseTest is " + area.IsWithinArea(outsideCloseTest));
        Debug.Log("outsideTest is " + area.IsWithinArea(outsideTest));
    }

    private void GPSPathTest() {

        List<GPSPoint> pathPoints = new List<GPSPoint>() {
            new GPSPoint(42.498211, -83.110359),// Gardenia Barrington
            new GPSPoint(42.496665, -83.110303),// Forest Barrington
       };

        GPSPath path = new GPSPath(pathPoints, 25f, 50f, new GPSPoint(42.498204, -83.110328));

        GPSPoint withinWidth = new GPSPoint(42.497699, -83.110335);
        GPSPoint withinSnapWidth = new GPSPoint(42.498040, -83.110798);
        //GPSPoint notNearPath = new GPSPoint(42.497681, -83.105547);

        Vector2 withinWidthRelative = GPSFunctions.GPSPointToMtrVector(withinWidth, path.mRefrencePoint);
        Vector2 withinSnapRelative = GPSFunctions.GPSPointToMtrVector(withinSnapWidth, path.mRefrencePoint);
        //Vector2 notNearRelative = GPSFunctions.GPSPointToMtrVector(notNearPath, path.mRefrencePoint);

        Vector2 withinResult = path.PositionOnPath(withinWidth);
        Vector2 snapResult = path.PositionOnPath(withinSnapWidth);
        //Vector2 notNearResult = path.PositionOnPath(notNearPath);

        Debug.Log("withinWidth was " + withinWidthRelative.x + ", " + withinWidthRelative.y + " now is " + withinResult.x + ", " + withinResult.y);
        Debug.Log("snapWidth was " + withinSnapRelative.x + ", " + withinSnapRelative.y + " now is " + snapResult.x + ", " + snapResult.y);
        //Debug.Log("notNear was " + notNearRelative.x + ", " + notNearRelative.y + " now is " + notNearResult.x + ", " + notNearResult.y);
    }

    IEnumerator PlayerIconTests() {

        //GPSPoint location1 = new GPSPoint(42.495443, -83.114151);//Upper RIght map corner
        //GPSPoint location2 = new GPSPoint(42.498211, -83.110349);//where Gardenia/Brrington
        //GPSPoint location3 = new GPSPoint(42.477929, -83.159748);//Palmer Ln
        //GPSPoint location4 = new GPSPoint(42.477421, -83.160687);//Gardenia Brettonwoods

        //GPSPoint corner1 = new GPSPoint(42.476626, -83.159314);
        //GPSPoint corner2 = new GPSPoint(42.477795, -83.161395);

        //mMapView.UpdatePlayerLocation(corner1);
        //yield return new WaitForSeconds(2.0f);

        //mMapView.UpdatePlayerLocation(corner2);
        //yield return new WaitForSeconds(2.0f);

        //mMapView.UpdatePlayerLocation(location1);
        //yield return new WaitForSeconds(1.0f);

        //mMapView.UpdatePlayerLocation(location2);
        //yield return new WaitForSeconds(2.0f);

        //mMapView.UpdatePlayerLocation(location3);
        //yield return new WaitForSeconds(2.0f);

        //mMapView.UpdatePlayerLocation(location4);
        yield return new WaitForSeconds(2.0f);
    }
}
#endif
