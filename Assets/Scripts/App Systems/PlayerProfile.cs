﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class PlayerProfile: MonoBehaviour {

    public static PlayerProfile instance;

    public ZooProfile mCurrentZoo;
    public StoryDataSystem mCurrentStory;
    public string mStoryTag = "story";

    public List<StoryDataSystem> mStoriesCompleted;    
    public List<TaskDataSystem> mTasksCompleted;

    private void Awake() {
        DontDestroyOnLoad(gameObject);

        

        if (instance != null && instance != this) {
            Destroy(this.gameObject);
        }
        else {
            instance = this;
        }
    }

    private void SetCurrentStory(StoryDataSystem story) {
        if(mCurrentStory != null) {
            mCurrentStory = story;
        }
       
    } 
    
    public void StartStory(StoryDataSystem story) {
        if (!story.mIsCompleted) {
            if (mCurrentStory == null) {
                mCurrentStory = story;
                mCurrentStory.mCurrentPage = 0;
            }


            if (mCurrentStory == story) {
                OpenCurrentStoryScene();
            }
            else {
                //This is where I warn the player htey already have a active current scene
            }
        }
    }  

    public void CurrentStoryCompleted() {
        mCurrentStory.mIsCompleted = true;
        mCurrentStory = null;
    }

    public void OpenCurrentStoryScene() {
        SceneManager.LoadScene(mCurrentStory.mSceneName);
    }

    public void NewSceneLoaded() {
        GameObject temp = GameObject.FindGameObjectWithTag(mStoryTag);

        Debug.Assert(temp != null);
        Debug.Assert(temp.GetComponent<StorySystem>() != null);
        temp.GetComponent<StorySystem>().mPlayerProfile = this;
                
    }

    //Eventually I'd like to check if this should be their home zoo, if not, select where.

    /*public static void NewData() {

        if (PlayerProfile.instance == null) {
            instance = new PlayerProfile();

            instance.mStoriesCompleted = new List<StorySystem>();
            instance.mTasksCompleted = new List<TaskSystem>();
     
        }
    }*/

    public void OnApplicationPause(bool pause) {
        SaveZooData();
    }

    public void OnApplicationQuit() {
        SaveZooData();
    }

    public void SaveZooData() {
        SaveAndLoad.mCurrentZooData = mCurrentZoo;
        SaveAndLoad.mSavedPlayerProfile = this;

        SaveAndLoad.Save();
    }
}
