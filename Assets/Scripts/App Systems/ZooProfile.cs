﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ZooProfile: ScriptableObject {

    public int mZooID;
    public GPSPolygon mGPSArea;

    public List<StoryDataSystem> mAllStories;
    public List<TaskDataSystem> mAllTasks;

    [HideInInspector] public List<StoryDataSystem> mAvailableStories;
    [HideInInspector] public List<TaskDataSystem> mAvailableTasks;

}
