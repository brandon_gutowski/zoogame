﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class OpenStoryScene : MonoBehaviour {

    public StoryDataSystem mStory;
    public string mPlayerProfTag = "GameManager";

    private PlayerProfile mPlayerProf;   

    public void OpenTheStoryScene() {
        Debug.Log(mPlayerProf);
        Debug.Assert(mPlayerProf != null);

        mPlayerProf.StartStory(mStory);
    }

    public void Awake() {       

        SceneManager.sceneLoaded += SceneLoaded;
        SceneLoaded(SceneManager.GetActiveScene(), LoadSceneMode.Single);//clunky and do not like this

        if (mStory.mIsCompleted) {
            this.gameObject.SetActive(false);
        }
    }

    public void OnDestroy() {
        SceneManager.sceneLoaded -= SceneLoaded;
    }

    private void SceneLoaded(Scene scene, LoadSceneMode mode) {
        GameObject gameManager = GameObject.FindGameObjectWithTag(mPlayerProfTag);

        mPlayerProf = gameManager.GetComponent<PlayerProfile>();
    }
}
