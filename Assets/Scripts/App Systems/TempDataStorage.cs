﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempDataStorage : ScriptableObject {

    [SerializeField] private List<ZooProfile> mZoosAvailable;
    public float mProximityToZoo = 1000f; //meters 


    public ZooProfile GetNearbyZoo() {
        ZooProfile tempZP = null;

        if (mZoosAvailable.Count > 0) {
            tempZP = mZoosAvailable[0];

            for (int i = 1; i < mZoosAvailable.Count; i++) {
                if (mZoosAvailable[i].mGPSArea.IsPlayerWithinArea()) {
                    tempZP = mZoosAvailable[i];
                    break;

                }else {
                    float distanceFromPlayer = mZoosAvailable[i].mGPSArea.DistanceFromPlayer();
                    if(distanceFromPlayer < mProximityToZoo) {
                        tempZP = mZoosAvailable[i];
                    }
                }
            }
        }      

        return tempZP;
    }
}
