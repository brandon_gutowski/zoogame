﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[System.Serializable]
public class StoryDataSystem: ScriptableObject {

    public int mZooID;
    public int mID;
    public string mSceneName;

    public bool mIsCompleted {
        get {
            return m_IsCompleted;
        }
        set {
            if (!m_IsCompleted) {
                m_IsCompleted = value;
            }
        }
    }

    public int mNumberOfPages;
    public int mCurrentPage;

    private bool m_IsCompleted = false;

    public StoryDataSystem(ZooProfile zProfile) {
        mZooID = zProfile.mZooID;
        mID = zProfile.mAllTasks.Count;
        m_IsCompleted = false;
    }



}
