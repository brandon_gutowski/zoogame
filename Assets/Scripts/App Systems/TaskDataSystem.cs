﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[System.Serializable]
public class TaskDataSystem: ScriptableObject {

    public int mZooID;
    public int mID;
    public bool mIsCompleted {
        get {
            return mIsCompleted;
        }
        set {
            if (!mIsCompleted) {
                mIsCompleted = value;
            }
        }
    }

    public TaskDataSystem(ZooProfile zProfile) {
        mZooID = zProfile.mZooID;
        mID = zProfile.mAllTasks.Count;
        mIsCompleted = false;
    }
}
