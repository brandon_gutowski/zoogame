﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class SaveAndLoad {

    public static PlayerProfile mSavedPlayerProfile;
    public static ZooProfile mCurrentZooData;

    private string mSaveLocation;

    public static void Save() {
        
        /*if(mCurrentZooData != null) {
           
            if (Application.platform == RuntimePlatform.IPhonePlayer) {
                System.Environment.SetEnvironmentVariable("MONO_REFLECTION_SERIALIZER", "yes");
            }

            mSavedPlayerProfile = PlayerProfile.instance;
            FileStream playerFile;
            FileStream zooFile;
            FileStream storyFile;
            FileStream taskFile;

            BinaryFormatter binaryFormatter = new BinaryFormatter();
        #if UNITY_ANDROID || UNITY_EDITOR


            playerFile = File.Create(Application.persistentDataPath + "/savedPlayerData.zog");
            binaryFormatter.Serialize(playerFile, mSavedPlayerProfile);
            playerFile.Close();

            /*storyFile = File.Create(Application.persistentDataPath + "/savedPlayerStoryData.zog");

            for (int i = 0; i < mSavedPlayerProfile.mStoriesCompleted.Count; i++) {
                binaryFormatter.Serialize(storyFile, mSavedPlayerProfile.mStoriesCompleted[i]);
            }
            storyFile.Close();

            taskFile = File.Create(Application.persistentDataPath + "/savedPlayerTaskData.zog");

            for (int i = 0; i < mSavedPlayerProfile.mTasksCompleted.Count; i++) {
                binaryFormatter.Serialize(taskFile, mSavedPlayerProfile.mTasksCompleted[i]);
            }
            taskFile.Close();

            //This is where the zoo data starts
            zooFile = File.Create(Application.persistentDataPath + "/savedZooData.zog");
            binaryFormatter.Serialize(zooFile, mCurrentZooData);
            zooFile.Close();

            /*storyFile = File.Create(Application.persistentDataPath + "/savedStoryData.zog");   
            
            for(int i = 0; i < mCurrentZooData.mAllStories.Count; i++) {
                binaryFormatter.Serialize(storyFile, mCurrentZooData.mAllStories[i]);
            }
            storyFile.Close();

            taskFile = File.Create(Application.persistentDataPath + "/savedTaskData.zog");

            for (int i = 0; i < mCurrentZooData.mAllStories.Count; i++) {
                binaryFormatter.Serialize(taskFile, mCurrentZooData.mAllTasks[i]);
            }
            taskFile.Close();

#endif

#if UNITY_IOS
				if (File.Exists (Application.persistentDataPath + "/savedPlayerData.sig")) {
						Debug.Log ("File Exists, recreating player data");

						File.Delete (Application.persistentDataPath + "/savedPlayerData.sig");
						playerFile = File.Create (Application.persistentDataPath + "/savedPlayerData.sig");
						binaryFormatter.Serialize (playerFile, mSavedData);
						playerFile.Close ();
				}

				if(File.Exists(Application.persistentDataPath + "/savedLevelData.sig")){
						Debug.Log ("File Exists, recreating level data");

						File.Delete (Application.persistentDataPath + "/savedLevelData.sig");
						levelFile = File.Create (Application.persistentDataPath + "/savedLevelData.sig");
			
						for (int i = 0; i < mLevelData.Count; i++) {
								binaryFormatter.Serialize (levelFile, mLevelData [i]);
						}
			
						levelFile.Close ();
				} 

				if (!File.Exists (Application.persistentDataPath + "/savedPlayerData.sig")) {
						Debug.Log ("File does not exist, creating player data");

						playerFile = File.Create (Application.persistentDataPath + "/savedPlayerData.sig");
						binaryFormatter.Serialize (playerFile, mSavedData);
						playerFile.Close ();
				}

				if(!File.Exists (Application.persistentDataPath + "/savedLevelData.sig")){
			   			Debug.Log ("File does not exist, creating level data");

						levelFile = File.Create (Application.persistentDataPath + "/savedLevelData.sig");
	
						for (int i = 0; i < mLevelData.Count; i++) {
								binaryFormatter.Serialize (levelFile, mLevelData [i]);
						}

						levelFile.Close ();
						}
#endif
        }*/
    }

    public static void Load() {
        
        /*if (File.Exists(Application.persistentDataPath + "/savedPlayerData.zog")) {

            if (Application.platform == RuntimePlatform.IPhonePlayer) {
                System.Environment.SetEnvironmentVariable("MONO_REFLECTION_SERIALIZER", "yes");
            }

            BinaryFormatter binaryFormatter = new BinaryFormatter();
            FileStream playerFile = File.Open(Application.persistentDataPath + "/savedPlayerData.zog", FileMode.Open, FileAccess.Read);

            SaveAndLoad.mSavedPlayerProfile = (PlayerProfile)binaryFormatter.Deserialize(playerFile);
            playerFile.Close();

            if (File.Exists(Application.persistentDataPath + "/savedPlayerStoryData.zog")) {

                long position = 0;
                while (true) {
                    StoryDataSystem story = ReadStoryFromPath(Application.persistentDataPath + "/savedPlayerStoryData.zog", ref position);
                    if (story == null) break;
                    SaveAndLoad.mSavedPlayerProfile.mStoriesCompleted.Add(story);
                }
            }

            if (File.Exists(Application.persistentDataPath + "/savedPlayerTaskData.zog")) {

                long position = 0;
                while (true) {
                    TaskDataSystem task = ReadTaskFromPath(Application.persistentDataPath + "/savedPlayerTaskData.zog", ref position);
                    if (task == null) break;
                    SaveAndLoad.mSavedPlayerProfile.mTasksCompleted.Add(task);
                }
            }

            PlayerProfile.instance = mSavedPlayerProfile;
        }

        if (File.Exists(Application.persistentDataPath + "/savedZooData.zog")) {

            BinaryFormatter binaryFormatter = new BinaryFormatter();
            FileStream zooFile = File.Open(Application.persistentDataPath + "/savedZooData.zog", FileMode.Open, FileAccess.Read);

            SaveAndLoad.mCurrentZooData = (ZooProfile)binaryFormatter.Deserialize(zooFile);
            PlayerProfile.instance.mCurrentZoo = mCurrentZooData;
            zooFile.Close();

            if (File.Exists(Application.persistentDataPath + "/savedStoryData.zog")) {

                long position = 0;
                while (true) {
                    StoryDataSystem story = ReadStoryFromPath(Application.persistentDataPath + "/savedStoryData.zog", ref position);
                    if (story == null) break;
                    SaveAndLoad.mCurrentZooData.mAllStories.Add(story);
                }
            }

            if (File.Exists(Application.persistentDataPath + "/savedTaskData.zog")) {

                long position = 0;
                while (true) {
                    TaskDataSystem task = ReadTaskFromPath(Application.persistentDataPath + "/savedTaskData.zog", ref position);
                    if (task == null) break;
                    SaveAndLoad.mCurrentZooData.mAllTasks.Add(task);
                }
            }

            PlayerProfile.instance.mCurrentZoo = mCurrentZooData;

        }*/
    }

    /*private static StoryDataSystem ReadStoryFromPath(string path, ref long position) {

        BinaryFormatter binaryFormatter = new BinaryFormatter();

        StoryDataSystem storyData = null;
        using (FileStream storyFile = File.Open(path, FileMode.Open, FileAccess.Read)) {
            if (position < storyFile.Length) {
                storyFile.Seek(position, SeekOrigin.Begin);
                storyData = (StoryDataSystem)binaryFormatter.Deserialize(storyFile);
                position = storyFile.Position;

            }
        }

        return storyData;

    }

    private static TaskDataSystem ReadTaskFromPath(string path, ref long position) {

        BinaryFormatter binaryFormatter = new BinaryFormatter();

        TaskDataSystem taskData = null;
        using (FileStream storyFile = File.Open(path, FileMode.Open, FileAccess.Read)) {
            if (position < storyFile.Length) {
                storyFile.Seek(position, SeekOrigin.Begin);
                taskData = (TaskDataSystem)binaryFormatter.Deserialize(storyFile);
                position = storyFile.Position;

            }
        }

        return taskData;

    }*/
}
