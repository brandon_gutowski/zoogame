﻿using UnityEditor;
using UnityEngine;

public class ScriptableAssetCreationButtons {

    [MenuItem("Assets/Create/StoryDataSystem")]
    public static void CreateStorySystem() {
        ScriptableObjectUtility.CreateAsset<StoryDataSystem>();
    }

    [MenuItem("Assets/Create/TaskDataSystem")]
    public static void CreateTaskSystem() {
        ScriptableObjectUtility.CreateAsset<TaskDataSystem>();
    }

    [MenuItem("Assets/Create/ZooProfile")]
    public static void CreateZooProfile() {
        ScriptableObjectUtility.CreateAsset<ZooProfile>();
    }
}
